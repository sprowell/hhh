% HHH(1) Version 1.0 | The HHH Binary Processor

# NAME

**hhh** — generates a hex dump or a binary file

# SYNOPSIS

| **hello** \[**-p** _files_] \[**-o** _output file_ ]
| **hello** \[_files_] \[**-o** _output file_ ]

# DESCRIPTION

HHH is a binary file processor.  It can perform two basic functions.

1. Generate a hexdump for a binary file
2. Process a description to generate a binary file

## Options

-h, --help

:   Print this usage information.

# FILES

# ENVIRONMENT

# BUGS

See GitLab Issues: <https://github.com/sprowell/hhh/issues>

# AUTHOR

Stacy Prowell <sprowell@gmail.com>

# SEE ALSO

**hexdump(1)**
