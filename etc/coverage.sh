#!/usr/bin/env bash
#
# Build or update covearge information.
# Use #[cfg_attr(tarpaulin, skip)] to skip lines in source.
#
# This uses tarpaulin to perform coverage testing.  Find tarpaulin here:
# https://crates.io/crates/cargo-tarpaulin
#
# Install tarpaulin with:
# $ cargo install cargo-tarpaulin
#
# This install may fail if the openssl development libraries are not installed.
# On Ubuntu this is the libssl-dev package.  If you have other issues, please
# record them here.
#
# The odd echo at the start is there to handle a few doc examples that require
# standard input.  This prevents "hanging" during the coverage testing. It does
# not impact "cargo test".

cargo build
echo "" | cargo tarpaulin --skip-clean --engine llvm --out xml --run-types tests --run-types doctests \
    && mv cobertura.xml cov.xml
exit $?
