// hhh
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/hhh

use std::process::Command;

/// The name of the environment variable that holds the short hash.
const SHORT_HASH: &str = "SHORT_HASH";

fn main() {
    // Pass the git short hash to the program at compile time.  To access
    // this from the program use `env!("GIT_SHORT_HASH")` to get a static
    // string slice.
    //
    // If git cannot run, GIT_FAILED is returned.  If the hash cannot be
    // read, GIT_NO_HASH is returned.
    match Command::new("git")
        .args(["rev-parse", "--short", "HEAD"])
        .output()
    {
        Ok(output) => match String::from_utf8(output.stdout) {
            Ok(hash) => {
                println!("cargo:rustc-env={}={}", SHORT_HASH, hash);
            }
            _ => {
                println!("cargo:rustc-env={}=GIT_NO_HASH", SHORT_HASH);
            }
        },
        _ => {
            println!("cargo:rustc-env={}=GIT_FAILED", SHORT_HASH);
        }
    }
}
