# Project Roadmap

## Roadmap for 2.0.0 - File Import

  * `[ ]` Implement metadata-as-directive
  * `[ ]` Generate styled hex dumps
  * `[ ]` Generate CSV-ish
  * `[ ]` Import CSV-ish
  * `[ ]` Implement file import
  * `[ ]` Fix the man page (how to install?)
  * `[ ]` Build deb (cargo-deb)
  * `[ ]` Build rpm (cargo-rpm)
  * `[ ]` Build wix (cargo-wix)
  * `[ ]` Empty tech debt
  * `[ ]` Fix all FIXME items
  * `[ ]` Check licenses
  * `[ ]` Documentation tests wherever possible
  * `[ ]` Hit 95% coverage testing
  * `[ ]` build.sh success
  * `[ ]` Commit to GitLab and tests run automagically

## Roadmap for 1.0.0 - Initial Release

  * `[X]` Generate binaries
  * `[X]` Implement parsing byte sequences
  * `[X]` Factor the parsing library out into its own project
  * `[X]` Fix constants
  * `[X]` Implement shift operators
  * `[X]` Assign constant values with the equal sign
  * `[X]` Generate hexdumps
  * `[X]` Allow expressions in directive arguments
  * `[X]` Implement rebase relative and rebase absolute
  * `[X]` Make the directive implementation responsible for parsing its argument
  * `[X]` Write to standard output
  * `[X]` Allow variables in expressions
  * `[X]` Examples in the book are tested
  * `[X]` Consider letting the user specify a limit for offsets
  * `[X]` Implement parsing the config file
  * `[X]` Complete the book
  * `[X]` Empty tech debt
  * `[X]` Fix all FIXME items
  * `[X]` Check licenses
  * `[X]` Documentation tests wherever possible
  * `[X]` Hit 90% coverage testing
  * `[X]` build.sh success
  * `[X]` Commit to GitLab and tests run automagically

## Wish List

This is a parking lot for things that "would be nice to have."

  * `[ ]` Allow Python-like comments
  * `[ ]` See if it makes sense to integrate somehow with Katai Struct (https://kaitai.io/)
  * `[ ]` Permit sequences of numbers in expression brackets (problematic under current definitions)
  * `[ ]` Implement switching from stoic by resetting the state
