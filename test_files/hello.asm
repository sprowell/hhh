; mk: $mkAS && $mkDYN

section .text
global _start
_start:
    mov eax, 1
    mov edi, 1
    lea rsi, [rel msg]
    mov edx, msg.len
    syscall
    mov eax, 60
    xor edi, edi
    syscall
    hlt

msg: db "Hello, world!", 10
.len equ $-msg

