# Configuration

At startup **hhh** tries to read a configuration file, unless the command line switch `--no-configuration-file` is given.  **hhh** expects to find this file in a location consistent with platform standards.  The configuration file is the place to make persistent changes to the way **hhh** operates.

To locate the specific configuration file being used, run **hhh** with debugging enabled by setting the environment variable `HHH_LOG` to `debug`.

``` bash
$ HHH_LOG=debug hhh
[2023-10-15T22:15:52Z DEBUG hhh] Command line: HhhArgs { bias: 0, bytes_per_group: 1, count: 0, directives: [], groups_per_line: 16, group_separator: " ", list_directives: false, little_endian: false, meta: false, no_ascii: false, no_offset: false, offset_limit: 1073741824, output: None, parse: false, set_meta: [], start: 0, skip_zeros: false, stoic: false, uppercase: false, radix_prefixes: false, offset_width: 8, last_offset: 0, variables: {}, bias_stack: [], files: [] }
[2023-10-15T22:15:52Z DEBUG hhh] Configuration file: "/home/.config/hhh/hhh.config"
```

In this case the file is located at `/home/.config/hhh/hhh.config`.

The content of the configuration file must be a sequence of directives.  Note that comments are allowed.  For example, the following would define a few variables for use later.

``` text
// Set a few variables.
set("desmond", 0x1000)
set("alicia", 0x2000)
set("carver", 0x3000)
```

Likewise, the following will disable the ASCII prefix unless explicitly turned back on with a directive.

``` text
// Disable ASCII preview.
no-ascii
```
