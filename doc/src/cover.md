<!-- trunk-ignore(markdownlint/MD041) -->
![hhh](logo.png)

# The hhh Binary Processor

Stacy Prowell (<mailto:sprowell+hhh@gmail.com>)

Version 1.0

----

<sub>This book, and portions of the code used to generate it, are Copyright &copy; by Stacy Prowell.  All rights reserved.  This book is distributed under the terms of the Creative Commons [Attribution-ShareAlike 4.0 International][by-sa] license.</sub>

[by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
