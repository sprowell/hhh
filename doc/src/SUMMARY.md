# hhh

- [Cover](cover.md)
- [Thanks](thanks.md)

## The hhh Binary Processor

- [Motivation](./motivation.md)
- [Generating Hex Dumps](./writing.md)
- [Reading Binary Descriptions](./reading.md)
- [Expressions](./expressions.md)
- [Directives](./directive.md)
- [Configuration](./configuration.md)
