# Expressions

Expressions can be used for the arguments of directives, and can appear in square brackets in a binary description to define a byte sequence.

```bash
$ hhh --no-ascii -D"bias(0x1000*4)" test_files/simple.bin
00004000: a4 21 41 3b fe cf 08 7c de af 67 ea
```

Within a binary description, expressions in square brackets can be used to specify a number, either for an offset or for a byte or sequence of bytes.  If a byte sequence is intended, a width specifier *should* follow the closing square bracket (or the default of eight bytes will be used).  Radix prefixes are enabled in the brackets, and decimal is assumed.

```hhh -p
0: [4 * 16 + 2]/2

4: [0b11110000_11000000 | 0b1]

// Result
// 00000000: 00 42 00 00 00 00 00 00 00 00 f0 c1
```

The following operators, in addition to parentheses, are allowed, in increasing priority.

|Operator                     |Meaning                                   |
|-----------------------------|------------------------------------------|
|`-` `+`                      |Subtraction and addition                  |
|`*` `/` `%`                  |Multiplication, division with truncation, and remainder |
|`<<` `>>` `>>>` `\|` `^` `&` |Left shift, arithmetic right shift, logical right shift, inclusive or, exclusive or, and |
|`**`                         |Exponentiation (right associative)        |

The values assigned to variables (see the section on [Variables](reading.md#variables)) can be referenced in an expression, but assignments, captures, width specifiers, etc., are not permitted.

An expression must always yield a single numerical value, and its value is limited to 8 bytes.

The value of an expression is allowed to be negative, and the result will be a two's complement negative value of the specified size.

```hhh -p
[-512]/4

// Result
// 00000000: ff ff fe 00
```
