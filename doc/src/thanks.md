# Thanks

This document was written in [Markdown], with the book itself being generated using [mdBook].  During development [mdbook-linkcheck] was used to verify the links.

**hhh** itself uses several packages.

- [chrono] is used to get the current UTC time and date
- [clap] is used to process the command line arguments
- [directories-next] is used to find the configuration directory
- [log] and [env-logger] are used to provide error and debugging output
- [num] provides arbitrary-precision numbers
- [tempfile] is used to create temporary files during testing
- [termsize] is used to get the terminal width
- [textwrap] is used to wrap text nicely
- [trivet] is used to implement the binary description language parser


[Markdown]: https://daringfireball.net/projects/markdown/syntax
[mdBook]: https://rust-lang.github.io/mdBook/
[mdbook-linkcheck]: https://github.com/Michael-F-Bryan/mdbook-linkcheck
[manpages]: https://eddieantonio.ca/blog/2015/12/18/authoring-manpages-in-markdown-with-pandoc/
[chrono]: https://crates.io/crates/chrono
[clap]: https://crates.io/crates/clap
[directories-next]: https://crates.io/crates/directories-next
[log]: https://crates.io/crates/log
[env-logger]: https://crates.io/crates/env_logger
[num]: https://crates.io/crates/num
[tempfile]: https://crates.io/crates/tempfile
[termsize]: https://crates.io/crates/termsize
[textwrap]: https://crates.io/crates/textwrap
[trivet]: https://crates.io/crates/trivet
