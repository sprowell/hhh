# hhh

This is a hexadecimal encoder/decoder for files but has a parser for reading descriptions and generating binary files.  See the book distributed with the source (in the `doc` folder) for details.

**hhh** uses [Semantic Versioning][].

Have you found a bug?  Submit it through the [hhh Issues][] tracker, and be sure to include (1) what happens, (2) what you *expect* to happen, (3) how to *reproduce* the bug, and (4) what *version* of hhh you are using.

## Installation

To install **hhh** locally, use `cargo install`.

```bash
$ cargo install hhh
    Updating crates.io index
  Downloaded hhh v1.0.0
  Downloaded 1 crate (94.1 KB) in 0.91s
  Installing hhh v1.0.0
    Finished release [optimized] target(s) in 16.28s
```

If you are installing from a local copy of the repository, you can use `cargo install --path=.`, instead.

## Online Documentation

For the master branch only:

- [The hhh Book](https://binary-tools.gitlab.io/hhh/book/)
- [hhh Rust API](https://binary-tools.gitlab.io/hhh/hhh/)

## Local Documentation

**If you have built the documentation locally**, then these link will take you to it.

- [Local hhh Book](book.html)
- [Local hhh Rust API](apidocs.html)

## Building

You need to have Rust 2021 edition installed to compile.  Compilation should be relatively simple.

    $ cargo build

This should build the hhh binary.

You might have a look at `etc/build.sh` and `etc\build.ps1`.  These are little scripts (in bash and powershell) that run the "pre-commit" pipeline.

## API Documentation

If you want API documentation run `cargo doc --nodeps`.

After generation, the API documentation should be available locally starting at [`target/doc/hhh/index.html`](target/doc/hhh/index.html).

## Security

Please submit sensitive security issues using the "This issue is confidential..." button below the issue description field in the issue tracker.

The code should be checked, periodically, against the known Rust security advisories.  This can be done with `cargo audit`.  You can install that with `cargo install cargo-audit`.

Consider installing [Trivy][].  This is a vulnerability scanner.

## Contribute

See [CONTRIBUTING][] for information about contributing to this project.

If you *are* interested in contributing, see the [Roadmap][], the [TechDebt][], and any `TODO` or `FIXME` comments in the code for some things that still need to be done.  Others may be working on them, so be sure to check in with the project.

## Copyright

**hhh** is Copyright (c) 2023 by Stacy Prowell (sprowell+hhh@gmail.com).  All rights reserved.  See the [Copyright][] and [License][] files for more information.

[Copyright]:        ./COPYRIGHT
[License]:          ./LICENSE
[TechDebt]:         ./TechDebt.md
[Roadmap]:          ./Roadmap.md
[CONTRIBUTING]:     ./CONTRIBUTING.md
[hhh Issues]:       https://gitlab.com/sprowell/hhh/-/issues
[Semantic Versioning]: https://semver.org/
[Trivy]:            https://aquasecurity.github.io/trivy/
