# Enumeration of the Binary Description Language

This may be out of date.  See the implementation that follows this (rather closely) in `reader.rs`.

## Comments and Directives

Comments and directives can occur almost anywhere in the stream.  Comments are processed immediately
by the tokenizer.

## Tokens

Tokens present in the stream are identified below.

|Symbol |Name       |Meaning                    |
|-------|-----------|---------------------------|
|`V`    |variable   |`'$' [a-zA-Z0-9_]+`        |
|`=`    |equal      |`'='`                      |
|`>`    |arrow      |`'->'`                     |
|`:`    |colon      |`':'`                      |
|`N`    |number     |`NUMBER`                   |
|`W`    |width      |`'/' DECIMAL ('le'|'be')?` |
|`D`    |directive  |`[[ .* ]]`                 |
|`(`    |left paren |`'('`                      |
|`)`    |right paren|`')' ('*' DECIMAL)?`       |
|`{`    |left brace |`'{'`                      |
|`}`    |right brace|`'}'`                      |

## Enumeration

The enumeration does not identify non-error responses.  A predicate P is required.

```text
_

#
V
= error             Unexpected equal sign; expected a variable name prior to this.
> error             Unexpected capture; expected a number or byte sequence prior to this to capture.
: error             Unexpected colon; expected a number prior to this for the offset.
N
W error             Unexpected width specification; expected a number prior to this.
D / = _
(
) error             Found closing parenthesis without a prior open parenthesis.
{ / = _
}[P] / = _
}[-P] error         Closing brace without opening brace.

# variable
V V / = _
V =
V > error           Cannot capture from a variable.
V : / = _
V N / = N
V W / = ( )
V D / = _
V ( / = (
V ) error           Found closing parenthesis without a prior open parenthesis.
V { / = _
V }[P] / = _
V }[-P] error       Closing brace without opening brace.

# number
N V / = V
N = error           Unexpected equal sign; expected a variable name prior to this.
N >
N : / = _
N N / = N
N W / = ( )
N D / = _
N ( / = (
N ) error           Found closing parenthesis without a prior open parenthesis.
N { / = _
N }[P] / = _
N }[-P] error         Closing brace without opening brace.

# open
( V
( = error           Parenthesized byte sequences cannot contain assignments.
( > error           Parenthesized byte sequences cannot contain captures.
( : error           Cannot set the offset in a parenthesized byte sequence.
( N
( W error           Unexpected width specification; expected a number prior to this.
( D / = (
( ( error           Cannot nest parenthesiezd byte sequences.
( )
( { error           Cannot use structure inside parentheses.
( } error           Cannot use structure inside parentheses.

# assignment
V = V
V = = error         Unexpected character following an equal sign; expected a number or byte sequence.
V = > error         Unexpected character following an equal sign; expected a number or byte sequence.
V = : error         Unexpected character following an equal sign; expected a number or byte sequence.
V = N
V = W error         Cannot assign a width specification to a variable.
V = D error         Cannot assign a directive to a variable.
V = (
V = ) error         Found closing parenthesis without a prior open parenthesis.
V = { error         Cannot assign a structure to a variable.
V = } error         Cannot assign a structure to a variable

# number_capture
N > V / = _
N > = error         Unexpected character following capture arrow; expected a variable name.
N > > error         Unexpected character following capture arrow; expected a variable name.
N > : error         Unexpected character following capture arrow; expected a variable name.
N > N error         Unexpected character following capture arrow; expected a variable name.
N > W error         Unexpected character following capture arrow; expected a variable name.
N > D error         Unexpected directive; cannot capture to a directive.
N > ( error         Unexpected character following capture arrow; expected a variable name.
N > ) error         Unexpected character following capture arrow; expected a variable name.
N = { error         Cannot capture to a structure; expected a variable
N = } error         Cannot capture to a structure; expected a variable

# open_variable
( V V / = ( V
( V = error         Parenthesized byte sequences cannot contain assignments.
( V > error         Parenthesized byte sequences cannot contain captures.
( V : error         Cannot set the offset in a parenthesized byte sequence.
( V N / = ( N
( V W / = (
( V D / = (
( V ( error         Cannot nest parenthesized byte sequences.
( V ) / = ( )
( V { error         Cannot use structure inside parentheses.
( V } error         Cannot use structure inside parentheses.

# open_number
( N V / = ( V
( N = error         Parenthesized byte sequences cannot contain assignments.
( N > error         Parenthesized byte sequences cannot contain captures.
( N : error         Cannot set the offset in a parenthesized byte sequence.
( N N / = ( N
( N W / = (
( N D / = (
( N ( error         Cannot nest parenthesized byte sequences.
( N ) / = ( )
( N { error         Cannot use structure inside parentheses.
( N } error         Cannot use structure inside parentheses.

# closed
( ) V / = V
( ) = error         Unexpected equal sign; expected a variable name prior to this. 
( ) >
( ) : error         Unexpected colon; expected a number prior to this for the offset.
( ) N / = N
( ) W error         Cannot apply a width specification to a parenthesized byte sequence.
( ) D / = _
( ) ( / = (
( ) ) error         Found closing parenthesis without a prior open parenthesis.
( ) { / = _
( ) }[P] / = _
( ) }[-P] error         Closing brace without opening brace.

# assignment_variable
V = V V / = V
V = V = error       Unexpected equal sign; assignments cannot be chained.
V = V > error       Unexpected capture arrow; cannot capture a variable used in an assignment.
V = V : error       A variable cannot be used in an assignment and also as an offset.
V = V N / = N
V = V W / = _
V = V D / = _
V = V ( / = (
V = V ) error       Found closing parenthesis without a prior open parenthesis.
V = V { / = _
V = V }[P] / = _
V = V }[-P] error   Closing brace without opening brace.

# assignment_number
V = N V / = V
V = N = error       Unexpected equal sign; assignments cannot be chained.
V = N > error       Unexpected capture arrow; cannot capture a number used in an assignment.
V = N : error       A number cannot be used in an assignment and also as an offset.
V = N N / = N
V = N W / = _
V = N D / = _
V = N ( / = (
V = N ) error       Found closing parenthesis without a prior open parenthesis.
V = N { / = _
V = N }[P] / = _
V = N }[-P] error   Closing brace without opening brace.

# assignment_open
V = ( V
V = ( = error       Parenthesized byte sequences cannot contain assignments.
V = ( > error       Parenthesized byte sequences cannot contain captures.
V = ( : error       Cannot set the offset in a parenthesized byte sequence.
V = ( N 
V = ( W error       Unexpected width specification; expected a number prior to this.
V = ( D / = V = (
V = ( ( error       Cannot nest parenthesized byte sequences.
V = ( ) / = _
V = ( { error       Cannot use structure inside parentheses.
V = ( } error       Cannot use structure inside parentheses.

# closed_capture
( ) > V / = _
( ) > = error       Unexpected character following capture arrow; expected a variable name.
( ) > > error       Unexpected character following capture arrow; expected a variable name.
( ) > : error       Unexpected character following capture arrow; expected a variable name.
( ) > N error       Unexpected character following capture arrow; expected a variable name.
( ) > W error       Unexpected character following capture arrow; expected a variable name.
( ) > D error       Unexpected directive; cannot capture to a directive.
( ) > ( error       Unexpected character following capture arrow; expected a variable name.
( ) > ) error       Unexpected character following capture arrow; expected a variable name.
( ) > { error       Cannot use capture to a structure.
( ) > } error       Cannot use capture to a structure.

# assignment_open_variable
V = ( V V / = V = ( V
V = ( V = error     Parenthesized byte sequences cannot contain assignments.
V = ( V > error     Parenthesized byte sequences cannot contain captures.
V = ( V : error     Cannot set the offset in a parenthesized byte sequence.
V = ( V N / = V = ( N
V = ( V W / = V = (
V = ( V D / = V = (
V = ( V ( error     Cannot nest parenthesized byte sequences.
V = ( V ) / = _
V = ( V { error     Cannot use structure inside parentheses.
V = ( V } error     Cannot use structure inside parentheses.

# assignment_open_number
V = ( N V / = V = ( V
V = ( N = error     Parenthesized byte sequences cannot contain assignments.
V = ( N > error     Parenthesized byte sequences cannot contain captures.
V = ( N : error     Cannot set the offset in a parenthesized byte sequence.
V = ( N N / = V = ( N
V = ( N W / = V = (
V = ( N D / = V = (
V = ( N ( error     Cannot nest parenthesized byte sequences.
V = ( N ) / = _
V = ( N { error     Cannot use structure inside parentheses.
V = ( N } error     Cannot use structure inside parentheses.
```

## Predicates

P[_] = true
P[u{] = false
P[u}] = true iff C[u] > 0
P[u}] = false iff C[u] <= 0

## Additional Prefix-Recursive Functions

C[_] = 0
C[u{] = C[u] + 1 iff R[u{] != error
C[u{] = C[u] iff R[u{] == error
C[u}] = C[u] - 1 iff R[u}] != error
C[u}] = C[u] iff R[u}] == error

## Canonical Sequences

The following unreduced sequences represent the different parsing states.

```
_           #ready
V           #variable
N           #number
(           #open
V =         #assignment
N >         #number_capture
( V         #open_variable
( N         #open_number
( )         #closed
V = V       #assignment_variable
V = N       #assignment_number
V = (       #assignment_open
( ) >       #closed_capture
V = ( V     #assignment_open_variable
V = ( N     #assignment_open_number
```

## Named State

This was to be used, but was then discarded in favor of a more direct implementation.

|Sequence      |Name                         |Context       |Assignment      |Sequence      |Closed |Capture |
|--------------|-----------------------------|--------------|----------------|--------------|-------|--------|
|`_`           |`#ready`                     |None          |false           |false         |false  |false   |
|`V`           |`#variable`                  |Variable      |false           |false         |       |        |
|`N`           |`#number`                    |Number        |false           |false         |       |false   |
|`(`           |`#open`                      |None          |false           |true          |false  |false   |
|`V =`         |`#assignment`                |None          |true            |false         |false  |false   |
|`N >`         |`#number_capture`            |Number        |false           |false         |       |true    |
|`( V`         |`#open_variable`             |Variable      |false           |true          |       |        |
|`( N`         |`#open_number`               |Number        |false           |true          |       |false   |
|`( )`         |`#closed`                    |None          |false           |true          |true   |false   |
|`V = V`       |`#assignment_variable`       |Variable      |true            |false         |       |        |
|`V = N`       |`#assignment_number`         |Number        |true            |false         |       |false   |
|`V = (`       |`#assignment_open`           |None          |true            |true          |false  |false   |
|`( ) >`       |`#closed_capture`            |None          |false           |true          |true   |true    |
|`V = ( V`     |`#assignment_open_variable`  |Variable      |true            |true          |       |        |
|`V = ( N`     |`#assignment_open_number`    |Number        |true            |true          |       |false   |
