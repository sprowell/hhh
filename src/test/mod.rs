// hhh
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/hhh

//! Library and integration tests.

// The CLI tests are unreliable and do not show up in the coverage statistics.
// If this can be fixed, then these should be turned back on.
//mod cli;
mod integration;
