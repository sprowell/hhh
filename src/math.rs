// hhh
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/hhh

//! Some fast math routines.

/// Convert an array of bytes to a u8, assuming the bytes are in big
/// endian order.
///
/// If `check` is true, then check to make sure the final value is in
/// range.  If not, `None` is returned.
pub fn bytes_to_u8(bytes: &[u8], check: bool) -> Option<u8> {
    let len = bytes.len();
    match len {
        0 => {
            if check {
                None
            } else {
                Some(0)
            }
        }
        1 => Some(bytes[len - 1]),
        _ => {
            if check {
                for byte in &bytes[0..len - 1] {
                    if *byte != 0u8 {
                        return None;
                    }
                }
            }
            Some(bytes[len - 1])
        }
    }
}

/// Convert an array of bytes to a u16, assuming the bytes are in big
/// endian order.
///
/// If `check` is true, then check to make sure the final value is in
/// range.  If not, `None` is returned.
pub fn bytes_to_u16(bytes: &[u8], check: bool) -> Option<u16> {
    let len = bytes.len();
    match len {
        0 => {
            if check {
                None
            } else {
                Some(0)
            }
        }
        1 => Some(u16::from_be_bytes([0u8, bytes[len - 1]])),
        2 => Some(u16::from_be_bytes([bytes[len - 2], bytes[len - 1]])),
        _ => {
            if check {
                for byte in &bytes[0..len - 2] {
                    if *byte != 0u8 {
                        return None;
                    }
                }
            }
            Some(u16::from_be_bytes([bytes[len - 2], bytes[len - 1]]))
        }
    }
}

/// Convert an array of bytes to a u32, assuming the bytes are in big
/// endian order.
///
/// If `check` is true, then check to make sure the final value is in
/// range.  If not, `None` is returned.
pub fn bytes_to_u32(bytes: &[u8], check: bool) -> Option<u32> {
    let len = bytes.len();
    match len {
        0 => {
            if check {
                None
            } else {
                Some(0)
            }
        }
        1 => Some(u32::from_be_bytes([0u8, 0u8, 0u8, bytes[len - 1]])),
        2 => Some(u32::from_be_bytes([
            0u8,
            0u8,
            bytes[len - 2],
            bytes[len - 1],
        ])),
        3 => Some(u32::from_be_bytes([
            0u8,
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        4 => Some(u32::from_be_bytes([
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        _ => {
            if check {
                for byte in &bytes[0..len - 4] {
                    if *byte != 0u8 {
                        return None;
                    }
                }
            }
            Some(u32::from_be_bytes([
                bytes[len - 4],
                bytes[len - 3],
                bytes[len - 2],
                bytes[len - 1],
            ]))
        }
    }
}

/// Convert an array of bytes to a u32, assuming the bytes are in big
/// endian order.
///
/// If `check` is true, then check to make sure the final value is in
/// range.  If not, `None` is returned.
pub fn bytes_to_u64(bytes: &[u8], check: bool) -> Option<u64> {
    let len = bytes.len();
    match len {
        0 => {
            if check {
                None
            } else {
                Some(0)
            }
        }
        1 => Some(u64::from_be_bytes([
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            bytes[len - 1],
        ])),
        2 => Some(u64::from_be_bytes([
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            bytes[len - 2],
            bytes[len - 1],
        ])),
        3 => Some(u64::from_be_bytes([
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        4 => Some(u64::from_be_bytes([
            0u8,
            0u8,
            0u8,
            0u8,
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        5 => Some(u64::from_be_bytes([
            0u8,
            0u8,
            0u8,
            bytes[len - 5],
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        6 => Some(u64::from_be_bytes([
            0u8,
            0u8,
            bytes[len - 6],
            bytes[len - 5],
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        7 => Some(u64::from_be_bytes([
            0u8,
            bytes[len - 7],
            bytes[len - 6],
            bytes[len - 5],
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        8 => Some(u64::from_be_bytes([
            bytes[len - 8],
            bytes[len - 7],
            bytes[len - 6],
            bytes[len - 5],
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        _ => {
            if check {
                for byte in &bytes[0..len - 4] {
                    if *byte != 0u8 {
                        return None;
                    }
                }
            }
            Some(u64::from_be_bytes([
                bytes[len - 8],
                bytes[len - 7],
                bytes[len - 6],
                bytes[len - 5],
                bytes[len - 4],
                bytes[len - 3],
                bytes[len - 2],
                bytes[len - 1],
            ]))
        }
    }
}

/// Convert an array of bytes to an i32, assuming the bytes are in big
/// endian order.
///
/// If `check` is true, then check to make sure the final value is in
/// range.  If not, `None` is returned.
pub fn bytes_to_i64(bytes: &[u8], check: bool) -> Option<i64> {
    let len = bytes.len();
    match len {
        0 => {
            if check {
                None
            } else {
                Some(0)
            }
        }
        1 => Some(i64::from_be_bytes([
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            bytes[len - 1],
        ])),
        2 => Some(i64::from_be_bytes([
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            bytes[len - 2],
            bytes[len - 1],
        ])),
        3 => Some(i64::from_be_bytes([
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        4 => Some(i64::from_be_bytes([
            0u8,
            0u8,
            0u8,
            0u8,
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        5 => Some(i64::from_be_bytes([
            0u8,
            0u8,
            0u8,
            bytes[len - 5],
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        6 => Some(i64::from_be_bytes([
            0u8,
            0u8,
            bytes[len - 6],
            bytes[len - 5],
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        7 => Some(i64::from_be_bytes([
            0u8,
            bytes[len - 7],
            bytes[len - 6],
            bytes[len - 5],
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        8 => Some(i64::from_be_bytes([
            bytes[len - 8],
            bytes[len - 7],
            bytes[len - 6],
            bytes[len - 5],
            bytes[len - 4],
            bytes[len - 3],
            bytes[len - 2],
            bytes[len - 1],
        ])),
        _ => {
            if check {
                for byte in &bytes[0..len - 4] {
                    if *byte != 0u8 {
                        return None;
                    }
                }
            }
            Some(i64::from_be_bytes([
                bytes[len - 8],
                bytes[len - 7],
                bytes[len - 6],
                bytes[len - 5],
                bytes[len - 4],
                bytes[len - 3],
                bytes[len - 2],
                bytes[len - 1],
            ]))
        }
    }
}

#[cfg(test)]
mod test {
    use crate::math::{bytes_to_i64, bytes_to_u16, bytes_to_u32, bytes_to_u64, bytes_to_u8};

    #[test]
    fn u8_test() {
        assert_eq!(bytes_to_u8(&[], false), Some(0));
        assert_eq!(bytes_to_u8(&[], true), None);
        assert_eq!(bytes_to_u8(&[0x00], false), Some(0));
        assert_eq!(bytes_to_u8(&[0x00], true), Some(0));
        assert_eq!(bytes_to_u8(&[0x00; 2], false), Some(0));
        assert_eq!(bytes_to_u8(&[0x00; 2], true), Some(0));
        assert_eq!(bytes_to_u8(&[0x83], false), Some(0x83));
        assert_eq!(bytes_to_u8(&[0x83], true), Some(0x83));
        assert_eq!(bytes_to_u8(&[0x83; 2], false), Some(0x83));
        assert_eq!(bytes_to_u8(&[0x83; 2], true), None);
    }

    #[test]
    fn u16_test() {
        assert_eq!(bytes_to_u16(&[], false), Some(0));
        assert_eq!(bytes_to_u16(&[], true), None);
        assert_eq!(bytes_to_u16(&[0x00], false), Some(0));
        assert_eq!(bytes_to_u16(&[0x00], true), Some(0));
        assert_eq!(bytes_to_u16(&[0x00; 2], false), Some(0));
        assert_eq!(bytes_to_u16(&[0x00; 2], true), Some(0));
        assert_eq!(bytes_to_u16(&[0x00; 3], false), Some(0));
        assert_eq!(bytes_to_u16(&[0x00; 3], true), Some(0));
        assert_eq!(bytes_to_u16(&[0x83], false), Some(0x83));
        assert_eq!(bytes_to_u16(&[0x83], true), Some(0x83));
        assert_eq!(bytes_to_u16(&[0x83, 0x41], false), Some(0x8341));
        assert_eq!(bytes_to_u16(&[0x83, 0x41], true), Some(0x8341));
        assert_eq!(bytes_to_u16(&[0x83, 0x41, 0xae], false), Some(0x41ae));
        assert_eq!(bytes_to_u16(&[0x83, 0x41, 0xae], true), None);
    }

    #[test]
    fn u32_test() {
        assert_eq!(bytes_to_u32(&[], true), None);
        assert_eq!(bytes_to_u32(&[], false), Some(0));
        assert_eq!(bytes_to_u32(&[0x00], false), Some(0));
        assert_eq!(bytes_to_u32(&[0x00], true), Some(0));
        assert_eq!(bytes_to_u32(&[0x00; 2], false), Some(0));
        assert_eq!(bytes_to_u32(&[0x00; 2], true), Some(0));
        assert_eq!(bytes_to_u32(&[0x00; 3], false), Some(0));
        assert_eq!(bytes_to_u32(&[0x00; 3], true), Some(0));
        assert_eq!(bytes_to_u32(&[0x00; 4], false), Some(0));
        assert_eq!(bytes_to_u32(&[0x00; 4], true), Some(0));
        assert_eq!(bytes_to_u32(&[0x00; 5], false), Some(0));
        assert_eq!(bytes_to_u32(&[0x00; 5], true), Some(0));
        assert_eq!(bytes_to_u32(&[0x83], false), Some(0x83));
        assert_eq!(bytes_to_u32(&[0x83], true), Some(0x83));
        assert_eq!(bytes_to_u32(&[0x83, 0x41], false), Some(0x8341));
        assert_eq!(bytes_to_u32(&[0x83, 0x41], true), Some(0x8341));
        assert_eq!(bytes_to_u32(&[0x83, 0x41, 0xae], false), Some(0x8341ae));
        assert_eq!(bytes_to_u32(&[0x83, 0x41, 0xae], true), Some(0x8341ae));
        assert_eq!(
            bytes_to_u32(&[0x83, 0x41, 0xae, 0x02], false),
            Some(0x8341ae02)
        );
        assert_eq!(
            bytes_to_u32(&[0x83, 0x41, 0xae, 0x02], true),
            Some(0x8341ae02)
        );
        assert_eq!(
            bytes_to_u32(&[0x83, 0x41, 0xae, 0x02, 0xfe], false),
            Some(0x41ae02fe)
        );
        assert_eq!(bytes_to_u32(&[0x83, 0x41, 0xae, 0x02, 0xfe], true), None);
    }

    #[test]
    fn u64_test() {
        assert_eq!(bytes_to_u64(&[], true), None);
        assert_eq!(bytes_to_u64(&[], false), Some(0));
        assert_eq!(bytes_to_u64(&[0x00], false), Some(0));
        assert_eq!(bytes_to_u64(&[0x00], true), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 2], false), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 2], true), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 3], false), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 3], true), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 4], false), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 4], true), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 5], false), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 5], true), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 6], false), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 6], true), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 7], false), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 7], true), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 8], false), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 8], true), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 9], false), Some(0));
        assert_eq!(bytes_to_u64(&[0x00; 9], true), Some(0));
        assert_eq!(bytes_to_u64(&[0x83], false), Some(0x83));
        assert_eq!(bytes_to_u64(&[0x83], true), Some(0x83));
        assert_eq!(bytes_to_u64(&[0x83, 0x41], false), Some(0x8341));
        assert_eq!(bytes_to_u64(&[0x83, 0x41], true), Some(0x8341));
        assert_eq!(bytes_to_u64(&[0x83, 0x41, 0xae], false), Some(0x8341ae));
        assert_eq!(bytes_to_u64(&[0x83, 0x41, 0xae], true), Some(0x8341ae));
        assert_eq!(
            bytes_to_u64(&[0x83, 0x41, 0xae, 0x02], false),
            Some(0x8341ae02)
        );
        assert_eq!(
            bytes_to_u64(&[0x83, 0x41, 0xae, 0x02], true),
            Some(0x8341ae02)
        );
        assert_eq!(
            bytes_to_u64(&[0x83, 0x41, 0xae, 0x02, 0xfe], false),
            Some(0x8341ae02fe)
        );
        assert_eq!(
            bytes_to_u64(&[0x83, 0x41, 0xae, 0x02, 0xfe], true),
            Some(0x8341ae02fe)
        );
        assert_eq!(
            bytes_to_u64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65], false),
            Some(0x8341ae02fe65)
        );
        assert_eq!(
            bytes_to_u64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65], true),
            Some(0x8341ae02fe65)
        );
        assert_eq!(
            bytes_to_u64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0], false),
            Some(0x8341ae02fe65c0)
        );
        assert_eq!(
            bytes_to_u64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0], true),
            Some(0x8341ae02fe65c0)
        );
        assert_eq!(
            bytes_to_u64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0, 0x07], false),
            Some(0x8341ae02fe65c007)
        );
        assert_eq!(
            bytes_to_u64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0, 0x07], true),
            Some(0x8341ae02fe65c007)
        );
        assert_eq!(
            bytes_to_u64(
                &[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0, 0x07, 0xd8],
                false
            ),
            Some(0x41ae02fe65c007d8)
        );
        assert_eq!(
            bytes_to_u64(
                &[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0, 0x07, 0xd8],
                true
            ),
            None
        );
    }

    #[test]
    fn i64_test() {
        assert_eq!(bytes_to_i64(&[], true), None);
        assert_eq!(bytes_to_i64(&[], false), Some(0));
        assert_eq!(bytes_to_i64(&[0x00], false), Some(0));
        assert_eq!(bytes_to_i64(&[0x00], true), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 2], false), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 2], true), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 3], false), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 3], true), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 4], false), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 4], true), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 5], false), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 5], true), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 6], false), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 6], true), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 7], false), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 7], true), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 8], false), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 8], true), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 9], false), Some(0));
        assert_eq!(bytes_to_i64(&[0x00; 9], true), Some(0));
        assert_eq!(bytes_to_i64(&[0x83], false), Some(0x83));
        assert_eq!(bytes_to_i64(&[0x83], true), Some(0x83));
        assert_eq!(bytes_to_i64(&[0x83, 0x41], false), Some(0x8341));
        assert_eq!(bytes_to_i64(&[0x83, 0x41], true), Some(0x8341));
        assert_eq!(bytes_to_i64(&[0x83, 0x41, 0xae], false), Some(0x8341ae));
        assert_eq!(bytes_to_i64(&[0x83, 0x41, 0xae], true), Some(0x8341ae));
        assert_eq!(
            bytes_to_i64(&[0x83, 0x41, 0xae, 0x02], false),
            Some(0x8341ae02)
        );
        assert_eq!(
            bytes_to_i64(&[0x83, 0x41, 0xae, 0x02], true),
            Some(0x8341ae02)
        );
        assert_eq!(
            bytes_to_i64(&[0x83, 0x41, 0xae, 0x02, 0xfe], false),
            Some(0x8341ae02fe)
        );
        assert_eq!(
            bytes_to_i64(&[0x83, 0x41, 0xae, 0x02, 0xfe], true),
            Some(0x8341ae02fe)
        );
        assert_eq!(
            bytes_to_i64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65], false),
            Some(0x8341ae02fe65)
        );
        assert_eq!(
            bytes_to_i64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65], true),
            Some(0x8341ae02fe65)
        );
        assert_eq!(
            bytes_to_i64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0], false),
            Some(0x8341ae02fe65c0)
        );
        assert_eq!(
            bytes_to_i64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0], true),
            Some(0x8341ae02fe65c0)
        );
        assert_eq!(
            bytes_to_i64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0, 0x07], false),
            Some(-0x7cbe51fd019a3ff9)
        );
        assert_eq!(
            bytes_to_i64(&[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0, 0x07], true),
            Some(-0x7cbe51fd019a3ff9)
        );
        assert_eq!(
            bytes_to_i64(
                &[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0, 0x07, 0xd8],
                false
            ),
            Some(0x41ae02fe65c007d8)
        );
        assert_eq!(
            bytes_to_i64(
                &[0x83, 0x41, 0xae, 0x02, 0xfe, 0x65, 0xc0, 0x07, 0xd8],
                true
            ),
            None
        );
    }
}
