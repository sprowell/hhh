// hhh
// Copyright (c) 2023 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/hhh

//! Parse a binary file description.

use crate::byte_data::Bytes;
use crate::directives::parse_and_do_directive;
use crate::expression::parse_expression_ws;
use crate::options::HhhArgs;
use num::bigint::Sign;
use num::traits::ToBytes;
use num::{BigInt, Num, ToPrimitive, Zero};
use std::io::Write;
use trivet::errors::{io_error, syntax_error};
use trivet::parser::{ParseError, ParseResult};
use trivet::{parse_from_path, parse_from_stdin, Loc, Parser as TrivetParser};

const HEXTABLE: [u8; 128] = [
    16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 16, 16, 16, 16, 16, 16, 16, 10, 11, 12, 13, 14, 15, 16, 16, 16,
    16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 10,
    11, 12, 13, 14, 15, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, 16, 16, 16,
];

/// Store a "number" or a number-like thing.  Numbers can represent byte sequences, or byte sequences can (sometimes)
/// represent numbers.
#[derive(PartialEq, Debug, Clone)]
enum NumberKind {
    /// A signed number value.  These are assumed to be eight bytes in width.
    Signed(i64),
    /// A large (unbounded) number value.
    Large(BigInt),
    /// A sequence of bytes.
    Sequence(Vec<u8>),
}

impl NumberKind {
    /// Get the value as a u64.  This truncates bytes as necessary.  If the content is a sequence, then
    /// big-endian order is assumed because this is (typically) going to be used to get an offset, and the
    /// endianness overrides do not apply to offsets, which are not (intended to be) byte sequences.
    ///
    /// If the number is negative, then a twos-complement version is returned.
    pub fn as_u64(&self) -> u64 {
        match self {
            Self::Signed(value) => *value as u64,
            Self::Large(value) => {
                // We have to reduce the value to a u64.
                (value & BigInt::from(usize::MAX)).to_u64().unwrap()
            }
            Self::Sequence(bytes) => {
                // We only care about the rightmost bytes.
                let len = bytes.len();
                let end = bytes.len().min((usize::BITS / 8) as usize);
                let start = 0;
                let mut newbytes = [0u8; 8];
                newbytes[8 - end..8 - start].copy_from_slice(&bytes[len - end..len - start]);
                u64::from_be_bytes(newbytes)
            }
        }
    }

    /// Generate a new instance with the given width.  The result is *always* a sequence, and the width is always
    /// padded or truncated at the big end.  This is important for sequences, which are assumed to be in big endian
    /// order.
    pub fn set_width(&self, width: usize) -> NumberKind {
        let mut ret = vec![0u8; width];
        match self {
            Self::Signed(value) => {
                let bytes = value.to_be_bytes();
                let len = bytes.len();
                let start = 0;
                let end = width.min(bytes.len());
                ret[width - end..width - start].copy_from_slice(&bytes[len - end..len - start]);
            }

            Self::Large(value) => {
                if value.sign() == Sign::Minus {
                    ret = vec![0xff; width];
                }
                let bytes = value.to_signed_bytes_be();
                let len = bytes.len();
                let start = 0;
                let end = width.min(bytes.len());
                ret[width - end..width - start].copy_from_slice(&bytes[len - end..len - start]);
            }

            Self::Sequence(bytes) => {
                let len = bytes.len();
                let start = 0;
                let end = width.min(bytes.len());
                ret[width - end..width - start].copy_from_slice(&bytes[len - end..len - start]);
            }
        }

        NumberKind::Sequence(ret)
    }

    /// Get the bytes of this number.  The bytes are returned in the specified order except.  For sequences, the
    /// order is inverted iff `little_endian` is `true`.
    pub fn as_bytes(&self, little_endian: bool) -> Vec<u8> {
        match self {
            Self::Signed(value) => {
                if little_endian {
                    value.to_le_bytes().to_vec()
                } else {
                    value.to_be_bytes().to_vec()
                }
            }

            Self::Large(value) => {
                // This code is to account for the fact that when the highest bit of a number
                // is set (the sign bit) then BigInt allocates an additional zero byte to avoid
                // ambituity.  We need to discard that extra byte.
                if value != &BigInt::zero() && (value.bits() % 8) == 0 {
                    if little_endian {
                        let bytes = value.to_le_bytes();
                        bytes[0..bytes.len() - 1].to_vec()
                    } else {
                        value.to_be_bytes()[1..].to_vec()
                    }
                } else if little_endian {
                    value.to_le_bytes().to_vec()
                } else {
                    value.to_be_bytes().to_vec()
                }
            }

            Self::Sequence(bytes) => {
                let mut ret = bytes.clone();
                if little_endian {
                    ret.reverse();
                }
                ret
            }
        }
    }

    /// Write the bytes directly into the given byte store.  This avoids potential byte copying.  The bytes are
    /// written in the specified order.  For sequences, the order is inverted if `little_endian` is `true`.
    pub fn write(&self, store: &mut Bytes, little_endian: bool) {
        match self {
            Self::Signed(value) => {
                if little_endian {
                    store.add_bytes(&value.to_le_bytes())
                } else {
                    store.add_bytes(&value.to_be_bytes())
                }
            }

            Self::Large(value) => {
                // This code is to account for the fact that when the highest bit of a number
                // is set (the sign bit) then BigInt allocates an additional zero byte to avoid
                // ambituity.  We need to discard that extra byte.
                if value != &BigInt::zero() && value.bits() % 8 == 0 {
                    if little_endian {
                        let bytes = value.to_le_bytes();
                        store.add_bytes(&bytes[0..bytes.len() - 1])
                    } else {
                        store.add_bytes(&value.to_be_bytes()[1..])
                    }
                } else if little_endian {
                    store.add_bytes(&value.to_le_bytes())
                } else {
                    store.add_bytes(&value.to_be_bytes())
                }
            }

            Self::Sequence(bytes) => {
                if little_endian {
                    let mut bytes = bytes.clone();
                    bytes.reverse();
                    store.add_bytes(&bytes);
                } else {
                    store.add_bytes(bytes)
                }
            }
        }
    }
}

/// Represent the primary tokens.
#[derive(PartialEq, Debug, Clone)]
enum Token {
    /// Variable with name.
    Variable(String),
    /// Equal sign.
    Equal,
    /// Map for capture.
    Map,
    /// Colon for offset.
    Colon,
    /// Some form of number.
    Number(NumberKind),
    /// A width specifier.
    Width(usize, bool),
    /// Open parenthesis.
    OpenParen,
    /// Close parenthesis.
    CloseParen(usize),
    /// The open brace.
    OpenBrace,
    /// The closing brace.
    CloseBrace,
    /// A directive.
    Directive(String),
    /// End of stream.
    Eos,
}

/// Parsing states.
#[derive(Debug)]
enum State {
    Ready,
    Variable,
    Number,
    Open,
    Closed,
    Assignment,
    NumberCapture,
    OpenVariable,
    OpenNumber,
    AssignmentVariable,
    AssignmentNumber,
    AssignmentOpen,
    ClosedCapture,
    AssignmentOpenVariable,
    AssignmentOpenNumber,
}

/// Read hexdumps from the given files and try to generate a binary.
pub fn read_hexdump<W: Write + 'static>(config: HhhArgs, sink: W) -> ParseResult<()> {
    let mut config = config;
    let mut sink = sink;

    // Create a new parser.  This moves the args to the parser that now owns them.
    let mut hexdump_parser = Reader::new(&config);

    // If there are no files specified try to read from standard input.
    if config.files.is_empty() {
        let mut parser = parse_from_stdin();
        hexdump_parser.parse(&mut parser)?;
    }

    // Read all the files.
    let files = std::mem::take(&mut config.files);
    for path in files {
        // Get all the file content.
        let mut parser = match parse_from_path(&path) {
            Ok(result) => result,
            Err(err) => return Err(io_error(Loc::Internal, err)),
        };
        hexdump_parser.parse(&mut parser)?;
    }

    // Write all bytes to the sink.
    let mut bytes = hexdump_parser.finish();
    bytes.finalize();
    let data = bytes.get_bytes();
    if let Err(err) = sink.write(&data) {
        return Err(io_error(Loc::Internal, err));
    }
    let _ = sink.flush();
    Ok(())
}

/// Parse a binary file description.
pub struct Reader {
    /// The bytes parsed so far.
    bytes: Bytes,

    /// Our copy of the configuration.
    config: HhhArgs,

    /// A stack holding bias values to implement structures.
    bias_stack: Vec<i64>,
}

impl Reader {
    /// Make a new parser.
    pub fn new(config: &HhhArgs) -> Self {
        Self {
            bytes: Bytes::new(),
            config: config.clone(),
            bias_stack: vec![],
        }
    }

    /// Local consume whitespace method.  This has less variation that the more complex method in trivet
    /// that supports lots of comment styles, so it should run faster.
    fn consume_ws(&self, parser: &mut TrivetParser) {
        loop {
            match parser.peek() {
                ' ' | '\t' | '\n' | '\r' => parser.consume(),
                '/' if parser.peek_offset(1) == '/' => {
                    while parser.peek() != '\n' && !parser.is_at_eof() {
                        parser.consume()
                    }
                }
                '/' if parser.peek_offset(1) == '*' => {
                    parser.consume_until("*/");
                }
                _ => break,
            }
        }
    }

    /// Turn a string of hex digits into a sequence of bytes.
    fn str_to_bytes(&self, string: &[u8]) -> Vec<u8> {
        let mut index = 0;
        let mut bytes = vec![];
        let mut ch1;
        let mut ch2;
        if string.len() % 2 == 1 {
            // The string is odd length.
            ch1 = HEXTABLE[(string[index] & 0x7f) as usize];
            index += 1;
            bytes.push(ch1);
        }
        while index < string.len() {
            ch1 = HEXTABLE[(string[index] & 0x7f) as usize];
            index += 1;
            ch2 = HEXTABLE[(string[index] & 0x7f) as usize];
            index += 1;
            bytes.push((ch1 << 4) | ch2);
        }
        bytes
    }

    /// Handle an error in the correct manner.  There are two cases.
    ///
    /// - If we are `stoic`, then consume all characters to the end of line and return the `ok` value
    /// - If we are not `stoic`, then return the `error` value
    ///
    /// Usually this can be handled by suffixing a call to this with a question mark `?` to return the
    /// error automatically, and by discarding the okay value.
    ///
    fn handle_error_ws<T>(
        &self,
        parser: &mut TrivetParser,
        ok: T,
        error: ParseError,
    ) -> ParseResult<T> {
        if self.config.stoic {
            parser.consume_until("\n");
            self.consume_ws(parser);
            Ok(ok)
        } else {
            Err(error)
        }
    }

    /// Finalize this parser.  The byte stream parsed is returned.  This consumes the
    /// parser.
    pub fn finish(self) -> Bytes {
        self.bytes
    }

    /// Get a variable's value.  This generates and handles an error if the variable is not assigned.
    /// If an error *is* generated but is suppressed, then an empty byte sequence is returned.
    fn get_variable_value(
        &mut self,
        loc: &Loc,
        parser: &mut TrivetParser,
        name: &str,
    ) -> ParseResult<Vec<u8>> {
        self.config.last_offset = self.bytes.get_offset();
        match self.config.get_variable(name) {
            Some(value) => Ok(value.clone()),
            None => self.handle_error_ws(
                parser,
                vec![],
                syntax_error(
                    loc.clone(),
                    &format!(
                        "The variable ${} is not assigned a value at this point.",
                        name
                    ),
                ),
            ),
        }
    }

    /// Set a variable's value.
    fn set_variable_value(
        &mut self,
        loc: &Loc,
        parser: &mut TrivetParser,
        name: &str,
        value: &[u8],
    ) -> ParseResult<()> {
        if name == "_" || name == "__" {
            return self.handle_error_ws(
                parser,
                (),
                syntax_error(loc.clone(), "Cannot set the value of $_ or $__."),
            );
        }
        self.config.set_variable(name, value);
        Ok(())
    }

    /// Execute a directive.
    fn do_directive(
        &mut self,
        loc: &Loc,
        parser: &mut TrivetParser,
        directive: &str,
    ) -> ParseResult<()> {
        self.config.last_offset = self.bytes.get_offset();
        match parse_and_do_directive(directive, &mut self.config) {
            Ok(None) => {}
            Ok(Some(error)) => {
                self.handle_error_ws(parser, (), syntax_error(loc.clone(), &error))?;
            }
            Err(error) => {
                self.handle_error_ws(parser, (), error)?;
            }
        }
        Ok(())
    }

    /// Parse a prefixed number.  A leading negative sign may have been parsed; iff so, then the
    /// `negative` argument is true.  On entry the parser is assumed to be pointing to the zero.
    /// Trailing whitespace is consumed.
    fn parse_prefixed_number_ws(
        &mut self,
        parser: &mut TrivetParser,
        negative: bool,
    ) -> ParseResult<NumberKind> {
        // Get the radix.
        let loc = parser.loc();
        let radix = match parser.peek_offset(1) {
            'x' => {
                parser.consume_n(2);
                16
            }
            'o' => {
                parser.consume_n(2);
                8
            }
            'b' => {
                parser.consume_n(2);
                2
            }
            _ => 10,
        };

        // Get the digits.
        let digits = parser.take_while_unless(|ch| ch.is_ascii_hexdigit(), |ch| ch == '_');

        // Compute the number.
        match BigInt::from_str_radix(&digits, radix) {
            Ok(value) => Ok(NumberKind::Large(if negative { -value } else { value })),
            Err(error) => Err(syntax_error(loc, &error.to_string())),
        }
    }

    /// Parse the next significant token and return it.  This method implicitly
    /// handles any directives it encounters.  On entry this is expected to point
    /// to a non-whitespace character.  No whitespace is consumed on exit.
    fn parse_token(&mut self, parser: &mut TrivetParser) -> ParseResult<Token> {
        while !parser.is_at_eof() {
            match parser.peek() {
                '-' => {
                    // Possible map.
                    if parser.peek_offset(1) == '>' {
                        parser.consume_n(2);
                        return Ok(Token::Map);
                    }

                    // Possible error.
                    if !self.config.radix_prefixes {
                        let loc = parser.loc();
                        parser.consume();
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "found stray hyphen; negatives are not allowed unless \
                                using radix prefixes; was this intended to be a capture token?",
                            ),
                        )?;
                        self.consume_ws(parser);
                        continue;
                    }

                    // Negative number.
                    parser.consume();
                    match self.parse_prefixed_number_ws(parser, true) {
                        Ok(value) => return Ok(Token::Number(value)),
                        Err(error) => {
                            self.handle_error_ws(parser, (), error)?;
                            self.consume_ws(parser);
                            continue;
                        }
                    }
                }

                '0' if self.config.radix_prefixes => {
                    // Number.
                    match self.parse_prefixed_number_ws(parser, false) {
                        Ok(value) => return Ok(Token::Number(value)),
                        Err(error) => {
                            self.handle_error_ws(parser, (), error)?;
                            self.consume_ws(parser);
                            continue;
                        }
                    }
                }

                ch if ch.is_ascii_hexdigit() => {
                    if self.config.radix_prefixes {
                        match self.parse_prefixed_number_ws(parser, false) {
                            Ok(value) => return Ok(Token::Number(value)),
                            Err(error) => {
                                self.handle_error_ws(parser, (), error)?;
                                self.consume_ws(parser);
                                continue;
                            }
                        }
                    } else {
                        // Get all the hexadecimal digits.
                        let digits =
                            parser.take_while_unless(|ch| ch.is_ascii_hexdigit(), |ch| ch == '_');

                        // Convert to bytes.  We always store sequences in the order to be written, so if the
                        // current order is little endian, we have to reverse the bytes.
                        if self.config.little_endian {
                            let mut bytes = self.str_to_bytes(digits.as_bytes());
                            bytes.reverse();
                            return Ok(Token::Number(NumberKind::Sequence(bytes)));
                        }
                        return Ok(Token::Number(NumberKind::Sequence(
                            self.str_to_bytes(digits.as_bytes()),
                        )));
                    }
                }

                '"' => {
                    // A quoted string.
                    let bytes = match parser.parse_string_match_delimiter_ws() {
                        Ok(value) => value,
                        Err(error) => {
                            self.handle_error_ws(parser, (), error)?;
                            self.consume_ws(parser);
                            continue;
                        }
                    }
                    .into_bytes();
                    return Ok(Token::Number(NumberKind::Sequence(bytes)));
                }

                '$' => {
                    // A variable name.
                    let loc = parser.loc();
                    parser.consume();
                    let name = parser.take_while(|ch| ch.is_alphanumeric() || ch == '_');
                    if name.is_empty() {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Missing variable name after dollar sign"),
                        )?;
                        self.consume_ws(parser);
                        continue;
                    } else {
                        return Ok(Token::Variable(name));
                    }
                }

                '=' => {
                    parser.consume();
                    return Ok(Token::Equal);
                }

                ':' => {
                    parser.consume();
                    return Ok(Token::Colon);
                }

                '/' => {
                    // A width specification.
                    parser.consume();
                    let digits = parser.take_while_unless(|ch| ch.is_ascii_digit(), |ch| ch == '_');
                    let loc = parser.loc();
                    let width = match digits.parse::<u8>() {
                        Ok(value) => value,
                        Err(error) => {
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(loc, &error.to_string()),
                            )?;
                            self.consume_ws(parser);
                            continue;
                        }
                    };
                    let next = parser.peek_n(2).to_lowercase();
                    let little_endian = match next.as_str() {
                        "le" => {
                            parser.consume_n(2);
                            true
                        }
                        "be" => {
                            parser.consume_n(2);
                            false
                        }
                        _ => self.config.little_endian,
                    };
                    return Ok(Token::Width(width as usize, little_endian));
                }

                '(' => {
                    parser.consume();
                    return Ok(Token::OpenParen);
                }

                ')' => {
                    parser.consume();
                    self.consume_ws(parser);
                    let mut repeat = 1;
                    if parser.peek_and_consume_ws('*') {
                        // Found repeat.
                        let digits =
                            parser.take_while_unless(|ch| ch.is_ascii_digit(), |ch| ch == '_');
                        let loc = parser.loc();
                        repeat = match digits.parse::<usize>() {
                            Ok(value) => value,
                            Err(error) => {
                                self.handle_error_ws(
                                    parser,
                                    (),
                                    syntax_error(loc, &error.to_string()),
                                )?;
                                self.consume_ws(parser);
                                continue;
                            }
                        };
                    }
                    return Ok(Token::CloseParen(repeat));
                }

                '[' => {
                    if parser.peek_offset(1) == '[' {
                        parser.consume_n(2);
                        self.consume_ws(parser);
                        let text = parser.take_until("]]");
                        return Ok(Token::Directive(text));
                    } else {
                        parser.consume();
                        self.consume_ws(parser);
                        self.config.last_offset = self.bytes.get_offset();
                        let result = parse_expression_ws(parser, &mut self.config);
                        if !parser.peek_and_consume(']') {
                            let ch = parser.peek();
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    parser.loc(),
                                    &format!(
                                        "Expected ] to end the expression, but found '{}'",
                                        ch
                                    ),
                                ),
                            )?;
                            self.consume_ws(parser);
                            continue;
                        }
                        let expr = match result {
                            Ok(value) => value,
                            Err(error) => {
                                self.handle_error_ws(parser, (), error)?;
                                self.consume_ws(parser);
                                continue;
                            }
                        };
                        return Ok(Token::Number(NumberKind::Signed(expr)));
                    }
                }

                '{' => {
                    parser.consume();
                    return Ok(Token::OpenBrace);
                }

                '}' => {
                    parser.consume();
                    return Ok(Token::CloseBrace);
                }

                _ => {
                    let ch = parser.peek();
                    let loc = parser.loc();
                    parser.consume();
                    self.handle_error_ws(
                        parser,
                        (),
                        syntax_error(loc, &format!("Found unexpected character: {}", ch)),
                    )?;
                    self.consume_ws(parser);
                    continue;
                }
            }
        }

        // Stream exhausted.
        Ok(Token::Eos)
    }

    /// Parse at root.
    pub fn parse(&mut self, parser: &mut TrivetParser) -> ParseResult<()> {
        self.consume_ws(parser);
        // The current state.
        let mut state = State::Ready;
        // The last variable in the stream.
        let mut var = String::new();
        // A variable being assigned.
        let mut agn = String::new();
        // The last number in the stream.
        let mut num = NumberKind::Signed(0);
        // The sequence being collected.  Always reset on legal open paren.
        let mut seq = vec![];

        loop {
            // Obtain the next token from the stream.
            self.consume_ws(parser);
            let loc = parser.loc();
            let token = match self.parse_token(parser) {
                Ok(token) => token,
                Err(error) => {
                    self.handle_error_ws(parser, (), error)?;
                    continue;
                }
            };

            match state {
                State::Ready => {
                    match token {
                        Token::Eos => {
                            break;
                        }
                        Token::Variable(name) => {
                            var = name;
                            state = State::Variable;
                        }
                        Token::Equal => {
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected equal sign; expected a variable name prior to this."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Map => {
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected capture arrow; expected a number or byte sequence prior to this to capture."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Colon => {
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected colon; expected a number prior to this for the offset."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Number(number) => {
                            num = number;
                            state = State::Number;
                        }
                        Token::Width(_, _) => {
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected width specification; expected a number prior to this."
                            ))?;
                            state = State::Ready;
                        }
                        Token::OpenParen => {
                            seq.clear();
                            state = State::Open;
                        }
                        Token::CloseParen(_) => {
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Found closing parenthesis without a prior open parenthesis.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::OpenBrace => {
                            self.bias_stack.push(self.config.bias);
                            self.config.bias = -(self.bytes.get_offset() as i64);
                            state = State::Ready;
                        }
                        Token::CloseBrace => {
                            if self.bias_stack.is_empty() {
                                self.handle_error_ws(parser, (), syntax_error(
                                    loc,
                                    "Found closing brace } without a corresponding opening brace {."
                                ))?;
                            } else {
                                self.config.bias = self.bias_stack.pop().unwrap();
                            }
                            state = State::Ready;
                        }
                        Token::Directive(text) => {
                            self.do_directive(&loc, parser, &text)?;
                            state = State::Ready;
                        }
                    }
                }

                State::Variable => {
                    match token {
                        Token::Eos => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.bytes.add_bytes(&value);
                            break;
                        }
                        Token::Variable(name) => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.bytes.add_bytes(&value);
                            var = name;
                            state = State::Variable;
                        }
                        Token::Equal => {
                            agn = var.clone();
                            state = State::Assignment;
                        }
                        Token::Map => {
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(loc, "Cannot capture from a variable."),
                            )?;
                            state = State::Ready;
                        }
                        Token::Colon => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            let value_num = NumberKind::Sequence(value.clone());
                            let new_offset = value_num.as_u64();
                            let offset = if self.config.bias < 0 {
                                // Add the negative of the bias.
                                new_offset + (-self.config.bias as u64)
                            } else {
                                // Subtract the bias.
                                match new_offset.checked_sub(self.config.bias as u64) {
                                    Some(value) => value,
                                    None => {
                                        self.handle_error_ws(
                                            parser,
                                            (),
                                            syntax_error(
                                                loc,
                                                &format!(
                                                    "Bias ({}) makes this offset ({}) negative.",
                                                    self.config.bias, new_offset
                                                ),
                                            ),
                                        )?;
                                        continue;
                                    }
                                }
                            };
                            if offset >= self.config.offset_limit {
                                // This is an unconditional error.
                                return Err(syntax_error(
                                    loc,
                                    &format!(
                                        "Offset {} exceeds the limit {}.",
                                        offset, self.config.offset_limit
                                    ),
                                ));
                            }
                            self.bytes.set_offset(offset);
                            state = State::Ready;
                        }
                        Token::Number(number) => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.bytes.add_bytes(&value);
                            num = number;
                            state = State::Number;
                        }
                        Token::Width(width, order) => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            let value_num = NumberKind::Sequence(value.clone()).set_width(width);
                            seq = value_num.as_bytes(order);
                            state = State::Closed;
                        }
                        Token::OpenParen => {
                            seq.clear();
                            state = State::Open;
                        }
                        Token::CloseParen(_) => {
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Found closing parenthesis without a prior open parenthesis.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::OpenBrace => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.bytes.add_bytes(&value);
                            self.bias_stack.push(self.config.bias);
                            self.config.bias = -(self.bytes.get_offset() as i64);
                            state = State::Ready;
                        }
                        Token::CloseBrace => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.bytes.add_bytes(&value);
                            if self.bias_stack.is_empty() {
                                self.handle_error_ws(parser, (), syntax_error(
                                    loc,
                                    "Found closing brace } without a corresponding opening brace {."
                                ))?;
                            } else {
                                self.config.bias = self.bias_stack.pop().unwrap();
                            }
                            state = State::Ready;
                        }
                        Token::Directive(text) => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.bytes.add_bytes(&value);
                            self.do_directive(&loc, parser, &text)?;
                            state = State::Ready;
                        }
                    }
                }

                State::Number => {
                    match token {
                        Token::Eos => {
                            num.write(&mut self.bytes, false);
                            break;
                        }
                        Token::Variable(name) => {
                            num.write(&mut self.bytes, false);
                            var = name;
                            state = State::Variable;
                        }
                        Token::Equal => {
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected equal sign; expected a variable name prior to this."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Map => {
                            state = State::NumberCapture;
                        }
                        Token::Colon => {
                            let new_offset = num.as_u64();
                            let offset = if self.config.bias < 0 {
                                // Add the negative of the bias.
                                new_offset + (-self.config.bias as u64)
                            } else {
                                // Subtract the bias.
                                match new_offset.checked_sub(self.config.bias as u64) {
                                    Some(value) => value,
                                    None => {
                                        self.handle_error_ws(
                                            parser,
                                            (),
                                            syntax_error(
                                                loc,
                                                &format!(
                                                    "Bias ({}) makes this offset ({}) negative.",
                                                    self.config.bias, new_offset
                                                ),
                                            ),
                                        )?;
                                        continue;
                                    }
                                }
                            };
                            if offset >= self.config.offset_limit {
                                // This is an unconditional error.
                                return Err(syntax_error(
                                    loc,
                                    &format!(
                                        "Offset {} exceeds the limit {}.",
                                        offset, self.config.offset_limit
                                    ),
                                ));
                            }
                            self.bytes.set_offset(offset);
                            state = State::Ready;
                        }
                        Token::Number(number) => {
                            num.write(&mut self.bytes, false);
                            num = number;
                            state = State::Number;
                        }
                        Token::Width(width, order) => {
                            seq = num.set_width(width).as_bytes(order);
                            state = State::Closed;
                        }
                        Token::OpenParen => {
                            num.write(&mut self.bytes, false);
                            seq.clear();
                            state = State::Open;
                        }
                        Token::CloseParen(_) => {
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Found closing parenthesis without a prior open parenthesis.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::OpenBrace => {
                            num.write(&mut self.bytes, false);
                            self.bias_stack.push(self.config.bias);
                            self.config.bias = -(self.bytes.get_offset() as i64);
                            state = State::Ready;
                        }
                        Token::CloseBrace => {
                            num.write(&mut self.bytes, false);
                            if self.bias_stack.is_empty() {
                                self.handle_error_ws(parser, (), syntax_error(
                                    loc,
                                    "Found closing brace } without a corresponding opening brace {."
                                ))?;
                            } else {
                                self.config.bias = self.bias_stack.pop().unwrap();
                            }
                            state = State::Ready;
                        }
                        Token::Directive(text) => {
                            num.write(&mut self.bytes, false);
                            self.do_directive(&loc, parser, &text)?;
                            state = State::Ready;
                        }
                    }
                }

                State::Open => match token {
                    Token::Eos => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Incomplete sequence; missing closing parenthesis."),
                        )?;
                        break;
                    }
                    Token::Variable(name) => {
                        var = name;
                        state = State::OpenVariable;
                    }
                    Token::Equal => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain assignments.",
                            ),
                        )?;
                        state = State::Open;
                    }
                    Token::Map => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain captures.",
                            ),
                        )?;
                        state = State::Open;
                    }
                    Token::Colon => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Cannot set the offset in a parenthesized byte sequence.",
                            ),
                        )?;
                        state = State::Open;
                    }
                    Token::Number(number) => {
                        num = number;
                        state = State::OpenNumber;
                    }
                    Token::Width(_, _) => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Unexpected width specification; expected a number prior to this.",
                            ),
                        )?;
                        state = State::Open;
                    }
                    Token::OpenParen => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot nest parenthesiezd byte sequences."),
                        )?;
                        state = State::Open;
                    }
                    Token::CloseParen(times) => {
                        let mut newseq = vec![];
                        for _ in 0..times {
                            newseq.append(&mut seq.clone());
                        }
                        seq = newseq;
                        state = State::Closed;
                    }
                    Token::OpenBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::Open;
                    }
                    Token::CloseBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::Open;
                    }
                    Token::Directive(text) => {
                        self.do_directive(&loc, parser, &text)?;
                        state = State::Open;
                    }
                },

                State::Assignment => match token {
                    Token::Eos => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                &format!("Incomplete assignment to variable ${}.", var),
                            ),
                        )?;
                        break;
                    }
                    Token::Variable(name) => {
                        agn = var;
                        var = name;
                        state = State::AssignmentVariable;
                    }
                    Token::Equal => {
                        self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected extra equal sign following an equal sign; expected a number or byte sequence."
                            ))?;
                        state = State::Ready;
                    }
                    Token::Map => {
                        self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected width specifier following an equal sign; expected a number or byte sequence."
                            ))?;
                        state = State::Ready;
                    }
                    Token::Colon => {
                        self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected colon following an equal sign; expected a number or byte sequence."
                            ))?;
                        state = State::Ready;
                    }
                    Token::Number(number) => {
                        num = number;
                        state = State::AssignmentNumber;
                    }
                    Token::Width(_width, _order) => {
                        self.handle_error_ws(parser, (), syntax_error(
                            loc,
                            "Unexpected width specification following an equal sign; expected a number or byte sequence."
                        ))?;
                        state = State::Ready;
                    }
                    Token::OpenParen => {
                        seq.clear();
                        state = State::AssignmentOpen;
                    }
                    Token::CloseParen(_) => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Found closing parenthesis without a prior open parenthesis.",
                            ),
                        )?;
                        state = State::Ready;
                    }
                    Token::OpenBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot assign a structure to a variable."),
                        )?;
                        state = State::Ready;
                    }
                    Token::CloseBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot assign a structure to a variable."),
                        )?;
                        state = State::Ready;
                    }
                    Token::Directive(_) => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot assign a directive to a variable."),
                        )?;
                        state = State::Ready;
                    }
                },

                State::NumberCapture => {
                    match token {
                        Token::Eos => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected equal sign; expected a variable name prior to this."
                            ))?;
                            break;
                        }
                        Token::Variable(name) => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.set_variable_value(&loc, parser, &name, &bytes)?;
                            state = State::Ready;
                        }
                        Token::Equal => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected equal sign following capture arrow; expected a variable name."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Map => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected capture arrow following capture arrow; expected a variable name."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Colon => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected colon following capture arrow; expected a variable name."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Number(number) => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(loc, "Missing variable for capture."),
                            )?;
                            num = number;
                            state = State::Number;
                        }
                        Token::Width(_, _) => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected width specification following capture arrow; expected a variable name."
                            ))?;
                            state = State::Ready;
                        }
                        Token::OpenParen => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected open parenthesis following capture arrow; expected a variable name."
                            ))?;
                            state = State::Ready;
                        }
                        Token::CloseParen(_) => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected close parenthesis following capture arrow; expected a variable name."
                            ))?;
                            state = State::Ready;
                        }
                        Token::OpenBrace => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Cannot capture to a structure; expected a variable.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::CloseBrace => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Cannot capture to a structure; expected a variable.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::Directive(_) => {
                            let bytes = num.as_bytes(false);
                            self.bytes.add_bytes(&bytes);
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Cannot capture to a directive; expected a variable.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                    }
                }

                State::OpenVariable => match token {
                    Token::Eos => {
                        let value = self.get_variable_value(&loc, parser, &var)?;
                        seq.append(&mut value.clone());
                        self.bytes.add_bytes(&seq);
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Incomplete sequence; missing closing parenthesis."),
                        )?;
                        break;
                    }
                    Token::Variable(name) => {
                        let value = self.get_variable_value(&loc, parser, &var)?;
                        seq.append(&mut value.clone());
                        var = name;
                        state = State::OpenVariable;
                    }
                    Token::Equal => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain assignments.",
                            ),
                        )?;
                        state = State::Open;
                    }
                    Token::Map => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain captures.",
                            ),
                        )?;
                        state = State::Open;
                    }
                    Token::Colon => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Cannot set the offset in a parenthesized byte sequence.",
                            ),
                        )?;
                        state = State::Open;
                    }
                    Token::Number(number) => {
                        let value = self.get_variable_value(&loc, parser, &var)?;
                        seq.append(&mut value.clone());
                        num = number;
                        state = State::OpenNumber;
                    }
                    Token::Width(width, order) => {
                        let value = self.get_variable_value(&loc, parser, &var)?;
                        let num = NumberKind::Sequence(value.clone());
                        seq.append(&mut num.set_width(width).as_bytes(order));
                        state = State::Open;
                    }
                    Token::OpenParen => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot nest parenthesized byte sequences."),
                        )?;
                        state = State::Open;
                    }
                    Token::CloseParen(times) => {
                        let value = self.get_variable_value(&loc, parser, &var)?;
                        seq.append(&mut value.clone());
                        let mut newseq = vec![];
                        for _ in 0..times {
                            newseq.append(&mut seq.clone());
                        }
                        seq = newseq;
                        state = State::Closed;
                    }
                    Token::OpenBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::Open;
                    }
                    Token::CloseBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::Open;
                    }
                    Token::Directive(text) => {
                        let value = self.get_variable_value(&loc, parser, &var)?;
                        seq.append(&mut value.clone());
                        self.do_directive(&loc, parser, &text)?;
                        state = State::Open;
                    }
                },

                State::OpenNumber => match token {
                    Token::Eos => {
                        seq.append(&mut num.as_bytes(false));
                        self.bytes.add_bytes(&seq);
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Incomplete sequence; missing closing parenthesis."),
                        )?;
                        break;
                    }
                    Token::Variable(name) => {
                        seq.append(&mut num.as_bytes(false));
                        var = name;
                        state = State::OpenVariable;
                    }
                    Token::Equal => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain assignments.",
                            ),
                        )?;
                        state = State::Open;
                    }
                    Token::Map => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain captures.",
                            ),
                        )?;
                        state = State::Open;
                    }
                    Token::Colon => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Cannot set the offset in a parenthesized byte sequence.",
                            ),
                        )?;
                        state = State::Open;
                    }
                    Token::Number(number) => {
                        seq.append(&mut num.as_bytes(false));
                        num = number;
                        state = State::OpenNumber;
                    }
                    Token::Width(width, order) => {
                        seq.append(&mut num.set_width(width).as_bytes(order));
                        state = State::Open;
                    }
                    Token::OpenParen => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot nest parenthesized byte sequences."),
                        )?;
                        state = State::Open;
                    }
                    Token::CloseParen(times) => {
                        seq.append(&mut num.as_bytes(false));
                        let mut newseq = vec![];
                        for _ in 0..times {
                            newseq.append(&mut seq.clone());
                        }
                        seq = newseq;
                        state = State::Closed;
                    }
                    Token::OpenBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::Open;
                    }
                    Token::CloseBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::Open;
                    }
                    Token::Directive(text) => {
                        seq.append(&mut num.as_bytes(false));
                        self.do_directive(&loc, parser, &text)?;
                        state = State::Open;
                    }
                },

                State::Closed => {
                    match token {
                        Token::Eos => {
                            self.bytes.add_bytes(&seq);
                            break;
                        }
                        Token::Variable(name) => {
                            self.bytes.add_bytes(&seq);
                            var = name;
                            state = State::Variable;
                        }
                        Token::Equal => {
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected equal sign; expected a variable name prior to this."
                            ))?;
                            self.bytes.add_bytes(&seq);
                            state = State::Ready;
                        }
                        Token::Map => {
                            state = State::ClosedCapture;
                        }
                        Token::Colon => {
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected colon; expected a number prior to this for the offset."
                            ))?;
                            self.bytes.add_bytes(&seq);
                            state = State::Ready;
                        }
                        Token::Number(number) => {
                            self.bytes.add_bytes(&seq);
                            num = number;
                            state = State::Number;
                        }
                        Token::Width(_, _) => {
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Cannot apply a width specification to a parenthesized byte sequence."
                            ))?;
                            self.bytes.add_bytes(&seq);
                            state = State::Ready;
                        }
                        Token::OpenParen => {
                            self.bytes.add_bytes(&seq);
                            seq.clear();
                            state = State::Open;
                        }
                        Token::CloseParen(_) => {
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Found closing parenthesis without a prior open parenthesis.",
                                ),
                            )?;
                            self.bytes.add_bytes(&seq);
                            state = State::Ready;
                        }
                        Token::OpenBrace => {
                            self.bytes.add_bytes(&seq);
                            self.bias_stack.push(self.config.bias);
                            self.config.bias = -(self.bytes.get_offset() as i64);
                            state = State::Ready;
                        }
                        Token::CloseBrace => {
                            self.bytes.add_bytes(&seq);
                            if self.bias_stack.is_empty() {
                                self.handle_error_ws(parser, (), syntax_error(
                                    loc,
                                    "Found closing brace } without a corresponding opening brace {."
                                ))?;
                            } else {
                                self.config.bias = self.bias_stack.pop().unwrap();
                            }
                            state = State::Ready;
                        }
                        Token::Directive(text) => {
                            self.bytes.add_bytes(&seq);
                            self.do_directive(&loc, parser, &text)?;
                            state = State::Ready;
                        }
                    }
                }

                State::AssignmentVariable => {
                    match token {
                        Token::Eos => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            break;
                        }
                        Token::Variable(name) => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            var = name;
                            state = State::Variable;
                        }
                        Token::Equal => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Unexpected equal sign; assignments cannot be chained.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::Map => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected capture arrow; cannot capture a variable used in an assignment."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Colon => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "A variable cannot be used in an assignment and also as an offset."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Number(number) => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            num = number;
                            state = State::Number;
                        }
                        Token::Width(width, order) => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            let number = NumberKind::Sequence(value);
                            self.set_variable_value(
                                &loc,
                                parser,
                                &agn,
                                &number.set_width(width).as_bytes(order),
                            )?;
                            state = State::Ready;
                        }
                        Token::OpenParen => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            seq.clear();
                            state = State::Open;
                        }
                        Token::CloseParen(_) => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Found closing parenthesis without a prior open parenthesis.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::OpenBrace => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            self.bias_stack.push(self.config.bias);
                            self.config.bias = -(self.bytes.get_offset() as i64);
                            state = State::Ready;
                        }
                        Token::CloseBrace => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            if self.bias_stack.is_empty() {
                                self.handle_error_ws(parser, (), syntax_error(
                                    loc,
                                    "Found closing brace } without a corresponding opening brace {."
                                ))?;
                            } else {
                                self.config.bias = self.bias_stack.pop().unwrap();
                            }
                            state = State::Ready;
                        }
                        Token::Directive(text) => {
                            let value = self.get_variable_value(&loc, parser, &var)?;
                            self.set_variable_value(&loc, parser, &agn, &value)?;
                            self.do_directive(&loc, parser, &text)?;
                            state = State::Ready;
                        }
                    }
                }

                State::AssignmentNumber => {
                    match token {
                        Token::Eos => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            break;
                        }
                        Token::Variable(name) => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            var = name;
                            state = State::Variable;
                        }
                        Token::Equal => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Unexpected equal sign; assignments cannot be chained.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::Map => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected capture arrow; cannot capture a number used in an assignment."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Colon => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "A number cannot be used in an assignment and also as an offset."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Number(number) => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            num = number;
                            state = State::Number;
                        }
                        Token::Width(width, order) => {
                            self.set_variable_value(
                                &loc,
                                parser,
                                &agn,
                                &num.set_width(width).as_bytes(order),
                            )?;
                            state = State::Ready;
                        }
                        Token::OpenParen => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            seq.clear();
                            state = State::Open;
                        }
                        Token::CloseParen(_) => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Found closing parenthesis without a prior open parenthesis.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::OpenBrace => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            self.bias_stack.push(self.config.bias);
                            self.config.bias = -(self.bytes.get_offset() as i64);
                            state = State::Ready;
                        }
                        Token::CloseBrace => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            if self.bias_stack.is_empty() {
                                self.handle_error_ws(parser, (), syntax_error(
                                    loc,
                                    "Found closing brace } without a corresponding opening brace {."
                                ))?;
                            } else {
                                self.config.bias = self.bias_stack.pop().unwrap();
                            }
                            state = State::Ready;
                        }
                        Token::Directive(text) => {
                            self.set_variable_value(&loc, parser, &agn, &num.as_bytes(false))?;
                            self.do_directive(&loc, parser, &text)?;
                            state = State::Ready;
                        }
                    }
                }

                State::AssignmentOpen => match token {
                    Token::Eos => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Incomplete sequence; missing closing parenthesis."),
                        )?;
                        break;
                    }
                    Token::Variable(name) => {
                        var = name;
                        state = State::AssignmentOpenVariable;
                    }
                    Token::Equal => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain assignments.",
                            ),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Map => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain captures.",
                            ),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Colon => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Cannot set the offset in a parenthesized byte sequence.",
                            ),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Number(number) => {
                        num = number;
                        state = State::AssignmentOpenNumber;
                    }
                    Token::Width(_, _) => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Unexpected width specification; expected a number prior to this.",
                            ),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::OpenParen => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot nest parenthesized byte sequences."),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::CloseParen(times) => {
                        let mut newseq = vec![];
                        for _ in 0..times {
                            newseq.append(&mut seq.clone());
                        }
                        seq = newseq;
                        self.set_variable_value(&loc, parser, &var, &seq)?;
                        state = State::Ready;
                    }
                    Token::OpenBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::Open;
                    }
                    Token::CloseBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::Open;
                    }
                    Token::Directive(text) => {
                        self.do_directive(&loc, parser, &text)?;
                        state = State::AssignmentOpen;
                    }
                },

                State::ClosedCapture => {
                    match token {
                        Token::Eos => {
                            self.bytes.add_bytes(&seq);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Incomplete captures; expected a variable following the map arrow."
                            ))?;
                            break;
                        }
                        Token::Variable(name) => {
                            self.set_variable_value(&loc, parser, &name, &seq)?;
                            self.bytes.add_bytes(&seq);
                            state = State::Ready;
                        }
                        Token::Equal => {
                            self.bytes.add_bytes(&seq);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected equal sign following capture arrow; expected a variable name."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Map => {
                            self.bytes.add_bytes(&seq);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected capture arrow following a prior capture arrow; expected a variable name."
                            ))?;
                            state = State::ClosedCapture;
                        }
                        Token::Colon => {
                            self.bytes.add_bytes(&seq);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected colon following capture arrow; expected a variable name."
                            ))?;
                            state = State::Ready;
                        }
                        Token::Number(number) => {
                            self.bytes.add_bytes(&seq);
                            num = number;
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(loc, "Missing variable name for capture."),
                            )?;
                            state = State::Number;
                        }
                        Token::Width(_, _) => {
                            self.bytes.add_bytes(&seq);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected width following capture arrow; expected a variable name."
                            ))?;
                            state = State::Ready;
                        }
                        Token::OpenParen => {
                            self.bytes.add_bytes(&seq);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected open parenthesis following capture arrow; expected a variable name."
                            ))?;
                            state = State::Open;
                        }
                        Token::CloseParen(_) => {
                            self.bytes.add_bytes(&seq);
                            self.handle_error_ws(parser, (), syntax_error(
                                loc,
                                "Unexpected close parenthesis following capture arrow; expected a variable name."
                            ))?;
                            state = State::Ready;
                        }
                        Token::OpenBrace => {
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Cannot capture to a structure; expected a variable.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::CloseBrace => {
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Cannot capture to a structure; expected a variable.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                        Token::Directive(_) => {
                            self.handle_error_ws(
                                parser,
                                (),
                                syntax_error(
                                    loc,
                                    "Cannot capture to a directive; expected a variable.",
                                ),
                            )?;
                            state = State::Ready;
                        }
                    }
                }

                State::AssignmentOpenVariable => match token {
                    Token::Eos => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Incomplete sequence; missing closing parenthesis."),
                        )?;
                        break;
                    }
                    Token::Variable(name) => {
                        let mut value = self.get_variable_value(&loc, parser, &var)?;
                        seq.append(&mut value);
                        var = name;
                        state = State::AssignmentOpenVariable;
                    }
                    Token::Equal => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain assignments.",
                            ),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Map => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain captures.",
                            ),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Colon => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Cannot set the offset in a parenthesized byte sequence.",
                            ),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Number(number) => {
                        let mut value = self.get_variable_value(&loc, parser, &var)?;
                        seq.append(&mut value);
                        num = number;
                        state = State::AssignmentOpenNumber;
                    }
                    Token::Width(width, order) => {
                        let value = self.get_variable_value(&loc, parser, &var)?;
                        let thing = NumberKind::Sequence(value);
                        seq.append(&mut thing.set_width(width).as_bytes(order));
                        state = State::AssignmentOpen;
                    }
                    Token::OpenParen => {
                        let mut value = self.get_variable_value(&loc, parser, &var)?;
                        seq.append(&mut value);
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot nest parenthesized byte sequences."),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::CloseParen(times) => {
                        let mut value = self.get_variable_value(&loc, parser, &var)?;
                        seq.append(&mut value);
                        let mut newseq = vec![];
                        for _ in 0..times {
                            newseq.append(&mut seq.clone());
                        }
                        seq = newseq;
                        self.set_variable_value(&loc, parser, &var, &seq)?;
                        state = State::Ready;
                    }
                    Token::OpenBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::CloseBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Directive(text) => {
                        let mut value = self.get_variable_value(&loc, parser, &var)?;
                        seq.append(&mut value);
                        self.do_directive(&loc, parser, &text)?;
                        state = State::Ready;
                    }
                },

                State::AssignmentOpenNumber => match token {
                    Token::Eos => {
                        seq.append(&mut num.as_bytes(false));
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Incomplete sequence; missing closing parenthesis."),
                        )?;
                        break;
                    }
                    Token::Variable(name) => {
                        seq.append(&mut num.as_bytes(false));
                        var = name;
                        state = State::AssignmentOpenVariable;
                    }
                    Token::Equal => {
                        seq.append(&mut num.as_bytes(false));
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain assignments.",
                            ),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Map => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Parenthesized byte sequences cannot contain captures.",
                            ),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Colon => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(
                                loc,
                                "Cannot set the offset in a parenthesized byte sequence.",
                            ),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Number(number) => {
                        seq.append(&mut num.as_bytes(false));
                        num = number;
                        state = State::AssignmentOpenNumber;
                    }
                    Token::Width(width, order) => {
                        seq.append(&mut num.set_width(width).as_bytes(order));
                        state = State::AssignmentOpen;
                    }
                    Token::OpenParen => {
                        seq.append(&mut num.as_bytes(false));
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot nest parenthesized byte sequences."),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::CloseParen(times) => {
                        seq.append(&mut num.as_bytes(false));
                        let mut newseq = vec![];
                        for _ in 0..times {
                            newseq.append(&mut seq.clone());
                        }
                        seq = newseq;
                        self.set_variable_value(&loc, parser, &var, &seq)?;
                        state = State::Ready;
                    }
                    Token::OpenBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::CloseBrace => {
                        self.handle_error_ws(
                            parser,
                            (),
                            syntax_error(loc, "Cannot use a structure inside parentheses."),
                        )?;
                        state = State::AssignmentOpen;
                    }
                    Token::Directive(text) => {
                        seq.append(&mut num.as_bytes(false));
                        self.do_directive(&loc, parser, &text)?;
                        state = State::Ready;
                    }
                },
            }
        }

        // If we terminate with the stack not empty, generate an error.  Otherwise unwind
        // the stack.
        if !self.bias_stack.is_empty() {
            self.handle_error_ws(
                parser,
                (),
                syntax_error(parser.loc(), "Structure was not closed with '}'"),
            )?;
            while !self.bias_stack.is_empty() {
                self.config.bias = self.bias_stack.pop().unwrap();
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod test {

    use crate::{byte_data::Bytes, options::HhhArgs, reader::Token};
    use num::{BigInt, FromPrimitive};
    use trivet::{parse_from_string, parser::ParseResult};

    use super::{NumberKind, Reader};

    #[test]
    fn number_kind_small_test() {
        let mut number = NumberKind::Signed(0);
        assert_eq!(number.as_u64(), 0);
        assert_eq!(number.as_bytes(false), &[0; 8]);
        assert_ne!(number.as_u64(), 1);
        number = number.set_width(16);
        assert_eq!(number.as_bytes(false), &[0u8; 16]);
        number = number.set_width(2);
        assert_eq!(number.as_bytes(true), &[0u8; 2]);
        number = NumberKind::Signed(897345); // 0xdb141
        assert_eq!(number.as_u64(), 897345);
        assert_eq!(
            number.as_bytes(false),
            &[0, 0, 0, 0, 0, 0x0du8, 0xb1u8, 0x41u8]
        );
        assert_ne!(number.as_u64(), 1);
        number = number.set_width(16);
        assert_eq!(
            number.as_bytes(true),
            &[0x41u8, 0xb1u8, 0x0du8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        );
        assert_eq!(
            number.as_bytes(false),
            &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x0du8, 0xb1u8, 0x41u8]
        );
        number = number.set_width(2);
        assert_eq!(number.as_bytes(true), &[0x41u8, 0xb1u8]);
        assert_eq!(number.as_bytes(false), &[0xb1u8, 0x41u8]);
        number = NumberKind::Signed(1098709903988745); // 0x3e7455589d809
        assert_eq!(number.as_u64(), 1098709903988745);
        assert_eq!(
            number.as_bytes(false),
            &[0u8, 0x03u8, 0xe7u8, 0x45u8, 0x55u8, 0x89u8, 0xd8u8, 0x09u8]
        );
        assert_ne!(number.as_u64(), 1);
        number = number.set_width(16);
        assert_eq!(
            number.as_bytes(false),
            &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0x03u8, 0xe7u8, 0x45u8, 0x55u8, 0x89u8, 0xd8u8, 0x09u8]
        );
        number = number.set_width(2);
        assert_eq!(number.as_bytes(false), &[0xd8u8, 0x09u8]);
        number = NumberKind::Large(BigInt::from(0xffffffffffffffffu64));
        assert_eq!(number.as_u64(), 0xffffffffffffffff);
        assert_eq!(number.as_bytes(true), &[0xff; 8]);
        assert_ne!(number.as_u64(), 1);
        number = number.set_width(16);
        assert_eq!(
            number.as_bytes(false),
            &[
                0, 0, 0, 0, 0, 0, 0, 0, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8,
                0xffu8
            ]
        );
        number = number.set_width(2);
        assert_eq!(number.as_bytes(true), &[0xffu8, 0xffu8]);
    }

    #[test]
    fn number_kind_large_nonnegative_test() {
        let mut number = NumberKind::Large(BigInt::from(0));
        assert_eq!(number.as_u64(), 0);
        assert_eq!(number.as_bytes(false), &[0; 1]);
        assert_ne!(number.as_u64(), 1);
        number = number.set_width(16);
        assert_eq!(number.as_bytes(false), &[0u8; 16]);
        number = number.set_width(2);
        assert_eq!(number.as_bytes(true), &[0u8; 2]);
        number = NumberKind::Large(BigInt::from(897345)); // 0xdb141
        assert_eq!(number.as_u64(), 897345);
        assert_eq!(number.as_bytes(false), &[0x0du8, 0xb1u8, 0x41u8]);
        assert_ne!(number.as_u64(), 1);
        number = number.set_width(16);
        assert_eq!(
            number.as_bytes(true),
            &[0x41u8, 0xb1u8, 0x0du8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        );
        number = number.set_width(2);
        assert_eq!(number.as_bytes(true), &[0x41u8, 0xb1u8]);
        number = NumberKind::Large(BigInt::from(1098709903988745u64)); // 0x3e7455589d809
        assert_eq!(number.as_u64(), 1098709903988745);
        assert_eq!(
            number.as_bytes(false),
            &[0x03u8, 0xe7u8, 0x45u8, 0x55u8, 0x89u8, 0xd8u8, 0x09u8]
        );
        assert_ne!(number.as_u64(), 1);
        number = number.set_width(16);
        assert_eq!(
            number.as_bytes(false),
            &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0x03u8, 0xe7u8, 0x45u8, 0x55u8, 0x89u8, 0xd8u8, 0x09u8]
        );
        number = number.set_width(2);
        assert_eq!(number.as_bytes(true), &[0x09u8, 0xd8u8]);
        number = NumberKind::Large(BigInt::from(0xffffffffffffffffu64));
        assert_eq!(number.as_u64(), 0xffffffffffffffff);
        assert_eq!(
            number.as_bytes(false),
            &[0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8]
        );
        assert_eq!(
            number.as_bytes(true),
            &[0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8]
        );
        assert_ne!(number.as_u64(), 1);
        number = number.set_width(16);
        assert_eq!(
            number.as_bytes(false),
            &[
                0, 0, 0, 0, 0, 0, 0, 0, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8, 0xffu8,
                0xffu8
            ]
        );
        number = number.set_width(2);
        assert_eq!(number.as_bytes(true), &[0xffu8, 0xffu8]);
    }

    #[test]
    fn number_kind_large_negative_test() {
        let mut number = NumberKind::Large(BigInt::from(-1));
        assert_eq!(number.as_u64(), 0xffffffffffffffffu64);
        assert_eq!(number.as_bytes(true), &[0xffu8; 1]);
        number = number.set_width(16);
        assert_eq!(number.as_bytes(true), &[0xffu8; 16]);
        number = number.set_width(2);
        assert_eq!(number.as_bytes(true), &[0xffu8; 2]);
        number = NumberKind::Large(BigInt::from(-897345)); // 0xdb141
        assert_eq!(number.as_u64(), 0xffffffffffffffffu64 - 897345u64 + 1);
        assert_eq!(
            number.as_bytes(false),
            &[0xff - 0x0d, 0xff - 0xb1, 0xff - 0x41 + 1]
        );
        assert_ne!(number.as_u64(), 1);
        number = number.set_width(16);
        assert_eq!(
            number.as_bytes(true),
            &[
                0xff - 0x41 + 1,
                0xff - 0xb1,
                0xff - 0x0d,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff
            ]
        );
        number = number.set_width(2);
        assert_eq!(number.as_bytes(true), &[0xff - 0x41 + 1, 0xff - 0xb1]);
        number = NumberKind::Large(BigInt::from(-1098709903988745i64)); // 0x3e7455589d809
        assert_eq!(
            number.as_u64(),
            0xffffffffffffffffu64 - 1098709903988745u64 + 1
        );
        assert_eq!(
            number.as_bytes(false),
            &[
                0xff - 0x03,
                0xff - 0xe7,
                0xff - 0x45,
                0xff - 0x55,
                0xff - 0x89,
                0xff - 0xd8,
                0xff - 0x09 + 1
            ]
        );
        assert_ne!(number.as_u64(), 1);
        number = number.set_width(16);
        assert_eq!(
            number.as_bytes(false),
            &[
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff,
                0xff - 0x03,
                0xff - 0xe7,
                0xff - 0x45,
                0xff - 0x55,
                0xff - 0x89,
                0xff - 0xd8,
                0xff - 0x09 + 1
            ]
        );
        number = number.set_width(2);
        assert_eq!(number.as_bytes(false), &[0xff - 0xd8, 0xff - 0x09 + 1]);
        let value = -BigInt::from(0xffffffffffffffffusize);
        number = NumberKind::Large(value);
        assert_eq!(number.as_u64(), 1);
    }

    #[test]
    fn number_kind_sequence_test() {
        let mut number = NumberKind::Sequence(vec![]);
        assert_eq!(number.as_bytes(true), &[]);
        assert_eq!(number.as_u64(), 0);
        number = number.set_width(3);
        assert_eq!(number.as_bytes(true), &[0, 0, 0]);
        assert_eq!(number.as_u64(), 0);
        number = NumberKind::Sequence(vec![0xffu8, 0xfeu8]);
        assert_eq!(number.as_bytes(true), &[0xfeu8, 0xffu8]);
        assert_eq!(number.as_u64(), 0xfffe);
        number = number.set_width(3);
        assert_eq!(number.as_bytes(true), &[0xfeu8, 0xffu8, 0u8]);
        assert_eq!(number.as_bytes(false), &[0u8, 0xffu8, 0xfeu8]);
    }

    #[test]
    fn test_tokens_1() {
        let args = HhhArgs::default();
        let mut parser = Reader::new(&args);
        let mut source = parse_from_string(
            r#"
            // This is not a valid binary description; it is just a test of the tokenizer.

            [[prefix]]          // Enable prefixes

            ->                  // Map
            -1                  // Negative number
            0xfe                // Valid hex number
            0o76                // Valid octal number
            255                 // Valid decimal number
            0b1010011           // Valid binary number

            -0b2                // Invalid binary number
            0o9                 // Invalid octal number

            [[no_prefix]]       // Disable prefixes

            -1                  // Generates an error

            0                   // Valid zero
            0000fe21438affef    // Valid 8 byte sequence
            fffefdfc            // Valid 4 byte sequence

            $chomper            // Valid variable
            $123                // Valid variable

            $                   // Error

            =                   // Equal
            :                   // Colon
            /25                 // Valid width
            /4le                // Valid width
            /2be                // Valid width
            /8LE                // Valid width
            /1BE                // Valid width
            (                   // Open paren
            )                   // Close paren
            [21]                // Expression

            "#,
        );
        parser.consume_ws(&mut source);

        // [[prefix]]
        let mut token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Directive("prefix".to_string()));
        parser.config.radix_prefixes = true;

        // ->
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Map);

        // -1
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Number(NumberKind::Large(BigInt::from(-1))));

        // 0xfe
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Number(NumberKind::Large(BigInt::from(0xfe))));

        // 0o76
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Number(NumberKind::Large(BigInt::from(0o76))));

        // 255
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Number(NumberKind::Large(BigInt::from(255))));

        // 0b1010011
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(
            token,
            Token::Number(NumberKind::Large(BigInt::from(0b1010011)))
        );

        // -0b2
        parser.consume_ws(&mut source);
        assert!(parser.parse_token(&mut source).is_err());

        // 0o9
        parser.consume_ws(&mut source);
        assert!(parser.parse_token(&mut source).is_err());

        // [[no_prefix]]
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Directive("no_prefix".to_string()));
        parser.config.radix_prefixes = false;

        // -1
        parser.consume_ws(&mut source);
        assert!(parser.parse_token(&mut source).is_err());
        source.consume_until("\n");

        // 0
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Number(NumberKind::Sequence(vec![0])));

        // 0000fe21438affef
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(
            token,
            Token::Number(NumberKind::Sequence(vec![
                0x00, 0x00, 0xfe, 0x21, 0x43, 0x8a, 0xff, 0xef
            ]))
        );

        // fffefdfc
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(
            token,
            Token::Number(NumberKind::Sequence(vec![0xff, 0xfe, 0xfd, 0xfc]))
        );

        // $chomper
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Variable("chomper".to_string()));

        // $123
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Variable("123".to_string()));

        // $
        parser.consume_ws(&mut source);
        assert!(parser.parse_token(&mut source).is_err());
        source.consume_until("\n");

        // -
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Equal);

        // :
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Colon);

        // /25
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Width(25, false));

        // /4le
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Width(4, true));

        // /2be
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Width(2, false));

        // /8LE
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Width(8, true));

        // 1BE
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Width(1, false));

        // (
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::OpenParen);

        // )
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::CloseParen(1));

        // [21]
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Number(NumberKind::Signed(21)));

        // End of stream
        parser.consume_ws(&mut source);
        token = parser.parse_token(&mut source).unwrap();
        assert_eq!(token, Token::Eos);
    }

    #[test]
    fn write_small_test() {
        let mut bytes = Bytes::new();
        NumberKind::Signed(0x214e3716_ffef001e).write(&mut bytes, true);
        assert_eq!(
            bytes.get_bytes(),
            vec![0x1e, 0x00, 0xef, 0xff, 0x16, 0x37, 0x4e, 0x21]
        );
        let mut bytes = Bytes::new();
        NumberKind::Signed(0x214e3716_ffef001e).write(&mut bytes, false);
        assert_eq!(
            bytes.get_bytes(),
            vec![0x21, 0x4e, 0x37, 0x16, 0xff, 0xef, 0x00, 0x1e]
        );
    }

    #[test]
    fn write_large_test() {
        let mut bytes = Bytes::new();
        NumberKind::Large(BigInt::from_i64(0x214e3716_ffef00).unwrap()).write(&mut bytes, true);
        assert_eq!(
            bytes.get_bytes(),
            vec![0x00, 0xef, 0xff, 0x16, 0x37, 0x4e, 0x21]
        );
        let mut bytes = Bytes::new();
        NumberKind::Large(BigInt::from_i64(0x814e3716_ffef00).unwrap()).write(&mut bytes, true);
        assert_eq!(
            bytes.get_bytes(),
            vec![0x00, 0xef, 0xff, 0x16, 0x37, 0x4e, 0x81]
        );
        let mut bytes = Bytes::new();
        NumberKind::Large(BigInt::from_i64(0x214e3716_ffef00).unwrap()).write(&mut bytes, false);
        assert_eq!(
            bytes.get_bytes(),
            vec![0x21, 0x4e, 0x37, 0x16, 0xff, 0xef, 0x00]
        );
        let mut bytes = Bytes::new();
        NumberKind::Large(BigInt::from_i64(0x814e3716_ffef00).unwrap()).write(&mut bytes, false);
        assert_eq!(
            bytes.get_bytes(),
            vec![0x81, 0x4e, 0x37, 0x16, 0xff, 0xef, 0x00]
        );
    }

    #[test]
    fn write_sequence_test() {
        let mut bytes = Bytes::new();
        NumberKind::Sequence(vec![0x21, 0x4e, 0x37, 0x16, 0xff, 0xef, 0x00])
            .write(&mut bytes, true);
        assert_eq!(
            bytes.get_bytes(),
            vec![0x00, 0xef, 0xff, 0x16, 0x37, 0x4e, 0x21]
        );
        let mut bytes = Bytes::new();
        NumberKind::Sequence(vec![0x21, 0x4e, 0x37, 0x16, 0xff, 0xef, 0x00])
            .write(&mut bytes, false);
        assert_eq!(
            bytes.get_bytes(),
            vec![0x21, 0x4e, 0x37, 0x16, 0xff, 0xef, 0x00]
        );
    }

    #[test]
    fn get_variable_value() {
        let mut source = parse_from_string("");
        let mut config = HhhArgs::default();
        config.set_variable("a", &[0u8, 17u8, 254u8, 18u8]);
        let mut parser = Reader::new(&config);
        parser
            .set_variable_value(&trivet::Loc::Internal, &mut source, "b", &[0u8])
            .unwrap();
        assert_eq!(
            parser
                .get_variable_value(&trivet::Loc::Internal, &mut source, "a")
                .unwrap(),
            vec![0u8, 17u8, 254u8, 18u8]
        );
        assert_eq!(
            parser
                .get_variable_value(&trivet::Loc::Internal, &mut source, "b")
                .unwrap(),
            vec![0u8]
        );
        assert!(parser
            .set_variable_value(&trivet::Loc::Internal, &mut source, "_", &[21u8, 17u8])
            .is_err());
        assert_eq!(
            parser
                .get_variable_value(&trivet::Loc::Internal, &mut source, "_")
                .unwrap(),
            vec![0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8]
        );
        assert!(parser
            .get_variable_value(&trivet::Loc::Internal, &mut source, "c")
            .is_err());
    }

    fn check(config: &mut HhhArgs, text: &str, bytes: &[u8]) {
        let mut source = parse_from_string(text);
        let mut parser = Reader::new(config);
        let _ = parser.parse(&mut source).unwrap();
        assert_eq!(&parser.finish().get_bytes(), bytes);
    }

    fn check_err(config: &mut HhhArgs, text: &str, bytes: &[u8]) {
        let mut source = parse_from_string(text);
        let mut parser = Reader::new(config);
        if config.stoic {
            let _ = parser.parse(&mut source).unwrap();
            assert_eq!(&parser.finish().get_bytes(), bytes);
        } else {
            assert!(parser.parse(&mut source).is_err());
        }
    }

    #[test]
    fn ready_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check(&mut config, "", &[]);
        check(&mut config, "$a", &[65]);
        check_err(&mut config, "=", &[]);
        check_err(&mut config, "->", &[]);
        check_err(&mut config, ":", &[]);
        check(&mut config, "20", &[0x20]);
        check_err(&mut config, "/2LE", &[]);
        check_err(&mut config, "(", &[]);
        check_err(&mut config, ")", &[]);
        check_err(&mut config, "{", &[]);
        check_err(&mut config, "}", &[]);

        Ok(())
    }

    #[test]
    fn variable_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check(&mut config, "$a", &[65]);
        check(&mut config, "$a $b", &[65, 4, 0]);
        check_err(&mut config, "$a =", &[]);
        check_err(&mut config, "$a ->", &[65]);
        check(&mut config, "$a :", &[]);
        check(&mut config, "$a 20", &[65, 0x20]);
        check(&mut config, "$a /2LE", &[65, 0]);
        check_err(&mut config, "$a (", &[]);
        check_err(&mut config, "$a )", &[65]);
        check_err(&mut config, "$a {", &[65]);
        check_err(&mut config, "$a }", &[65]);

        Ok(())
    }

    #[test]
    fn number_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check(&mut config, "72", &[0x72]);
        check(&mut config, "72 $b", &[0x72, 4, 0]);
        check_err(&mut config, "72 =", &[]);
        check_err(&mut config, "72 ->", &[0x72]);
        check(&mut config, "72 :", &[]);
        check(&mut config, "72 20", &[0x72, 0x20]);
        check(&mut config, "72 /2LE", &[0x72, 0]);
        check_err(&mut config, "72 (", &[0x72]);
        check_err(&mut config, "72 )", &[0x72]);
        check_err(&mut config, "72 {", &[0x72]);
        check_err(&mut config, "72 }", &[0x72]);

        Ok(())
    }

    #[test]
    fn open_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check_err(&mut config, "(", &[]);
        check_err(&mut config, "( $b", &[4, 0]);
        check_err(&mut config, "( =", &[]);
        check_err(&mut config, "( ->", &[]);
        check_err(&mut config, "( :", &[]);
        check_err(&mut config, "( 20", &[0x20]);
        check_err(&mut config, "( /2LE", &[]);
        check_err(&mut config, "( (", &[]);
        check(&mut config, "( )", &[]);
        check_err(&mut config, "( {", &[]);
        check_err(&mut config, "( }", &[]);

        Ok(())
    }

    #[test]
    fn assignment_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check_err(&mut config, "$a =", &[]);
        check(&mut config, "$a = $b", &[]);
        check_err(&mut config, "$a = =", &[]);
        check_err(&mut config, "$a = ->", &[]);
        check_err(&mut config, "$a = :", &[]);
        check(&mut config, "$a = 20", &[]);
        check_err(&mut config, "$a = /2LE", &[]);
        check_err(&mut config, "$a = (", &[]);
        check_err(&mut config, "$a = )", &[]);
        check_err(&mut config, "$a = {", &[]);
        check_err(&mut config, "$a = }", &[]);

        Ok(())
    }

    #[test]
    fn number_capture_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check(&mut config, "72 -> $b", &[0x72]);
        check_err(&mut config, "72 -> =", &[0x72]);
        check_err(&mut config, "72 -> ->", &[0x72]);
        check_err(&mut config, "72 -> :", &[0x72]);
        check_err(&mut config, "72 -> 20", &[0x72]);
        check_err(&mut config, "72 -> /2LE", &[0x72]);
        check_err(&mut config, "72 -> (", &[0x72]);
        check_err(&mut config, "72 -> )", &[0x72]);
        check_err(&mut config, "72 -> {", &[0x72]);
        check_err(&mut config, "72 -> }", &[0x72]);

        Ok(())
    }

    #[test]
    fn open_variable_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check_err(&mut config, "( $a $b", &[65, 4, 0]);
        check_err(&mut config, "( $a =", &[]);
        check_err(&mut config, "( $a ->", &[65]);
        check_err(&mut config, "( $a :", &[]);
        check_err(&mut config, "( $a 20", &[65, 0x20]);
        check_err(&mut config, "( $a /2LE", &[]);
        check_err(&mut config, "( $a (", &[65]);
        check(&mut config, "( $a )", &[65]);
        check_err(&mut config, "( $a {", &[65]);
        check_err(&mut config, "( $a }", &[65]);

        Ok(())
    }

    #[test]
    fn open_number_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check_err(&mut config, "( 72", &[0x72]);
        check_err(&mut config, "( 72 $b", &[4, 0]);
        check_err(&mut config, "( 72 =", &[0x72]);
        check_err(&mut config, "( 72 ->", &[0x72]);
        check_err(&mut config, "( 72 :", &[]);
        check_err(&mut config, "( 72 20", &[0x72, 0x20]);
        check_err(&mut config, "( 72 /2LE", &[]);
        check_err(&mut config, "( 72 (", &[0x72]);
        check(&mut config, "( 72 )", &[0x72]);
        check_err(&mut config, "( 72 {", &[0x72]);
        check_err(&mut config, "( 72 }", &[0x72]);

        Ok(())
    }

    #[test]
    fn closed_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check(&mut config, "( ) $b", &[4, 0]);
        check_err(&mut config, "( ) =", &[]);
        check_err(&mut config, "( ) ->", &[]);
        check_err(&mut config, "( ) :", &[]);
        check(&mut config, "( ) 20", &[0x20]);
        check_err(&mut config, "( ) /2LE", &[]);
        check_err(&mut config, "( ) (", &[]);
        check_err(&mut config, "( ) )", &[]);
        check_err(&mut config, "( ) {", &[]);
        check_err(&mut config, "( ) }", &[]);

        Ok(())
    }

    #[test]
    fn assignment_variable_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check(&mut config, "$a = $b", &[]);
        check(&mut config, "$a = $b $b", &[4, 0]);
        check_err(&mut config, "$a = $b =", &[]);
        check_err(&mut config, "$a = $b ->", &[]);
        check_err(&mut config, "$a = $b :", &[]);
        check(&mut config, "$a = $b 20", &[0x20]);
        check(&mut config, "$a = $b /2LE", &[]);
        check_err(&mut config, "$a = $b (", &[]);
        check_err(&mut config, "$a = $b )", &[]);
        check_err(&mut config, "$a = $b {", &[]);
        check_err(&mut config, "$a = $b }", &[]);

        Ok(())
    }

    #[test]
    fn assignment_number_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check(&mut config, "$a = 72", &[]);
        check(&mut config, "$a = 72 $b", &[4, 0]);
        check_err(&mut config, "$a = 72 =", &[]);
        check_err(&mut config, "$a = 72 ->", &[]);
        check_err(&mut config, "$a = 72 :", &[]);
        check(&mut config, "$a = 72 20", &[0x20]);
        check(&mut config, "$a = 72 /2LE", &[]);
        check_err(&mut config, "$a = 72 (", &[]);
        check_err(&mut config, "$a = 72 )", &[]);
        check_err(&mut config, "$a = 72 {", &[]);
        check_err(&mut config, "$a = 72 }", &[]);

        Ok(())
    }

    #[test]
    fn assignment_open_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check_err(&mut config, "$a = ( $b", &[]);
        check_err(&mut config, "$a = ( =", &[]);
        check_err(&mut config, "$a = ( ->", &[]);
        check_err(&mut config, "$a = ( :", &[]);
        check_err(&mut config, "$a = ( 20", &[]);
        check_err(&mut config, "$a = ( /2LE", &[]);
        check_err(&mut config, "$a = ( (", &[]);
        check(&mut config, "$a = ( )", &[]);
        check_err(&mut config, "$a = ( {", &[]);
        check_err(&mut config, "$a = ( }", &[]);

        Ok(())
    }

    #[test]
    fn closed_capture_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check(&mut config, "( ) -> $b", &[]);
        check_err(&mut config, "( ) -> =", &[]);
        check_err(&mut config, "( ) -> ->", &[]);
        check_err(&mut config, "( ) -> :", &[]);
        check_err(&mut config, "( ) -> 20", &[]);
        check_err(&mut config, "( ) -> /2LE", &[]);
        check_err(&mut config, "( ) -> (", &[]);
        check_err(&mut config, "( ) -> )", &[]);
        check_err(&mut config, "( ) -> {", &[]);
        check_err(&mut config, "( ) -> }", &[]);

        Ok(())
    }

    #[test]
    fn assignment_open_variable_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check_err(&mut config, "$a = ( $b $b", &[]);
        check_err(&mut config, "$a = ( $b =", &[]);
        check_err(&mut config, "$a = ( $b ->", &[]);
        check_err(&mut config, "$a = ( $b :", &[]);
        check_err(&mut config, "$a = ( $b 20", &[]);
        check_err(&mut config, "$a = ( $b /2LE", &[]);
        check_err(&mut config, "$a = ( $b (", &[]);
        check(&mut config, "$a = ( $b )", &[]);
        check_err(&mut config, "$a = ( $b {", &[]);
        check_err(&mut config, "$a = ( $b }", &[]);

        Ok(())
    }

    #[test]
    fn assignment_open_number_test() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.set_variable("a", &[65u8]);
        config.set_variable("b", &[4u8, 0u8]);

        check_err(&mut config, "$a = ( 71 $b", &[]);
        check_err(&mut config, "$a = ( 71 =", &[]);
        check_err(&mut config, "$a = ( 71 ->", &[]);
        check_err(&mut config, "$a = ( 71 :", &[]);
        check_err(&mut config, "$a = ( 71 20", &[]);
        check_err(&mut config, "$a = ( 71 /2LE", &[]);
        check_err(&mut config, "$a = ( 71 (", &[]);
        check(&mut config, "$a = ( 71 )", &[]);
        check_err(&mut config, "$a = ( 71 {", &[]);
        check_err(&mut config, "$a = ( 71 }", &[]);

        Ok(())
    }

    #[test]
    fn additional_parse_tests_1() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.stoic = true;
        config.set_variable("a", &[0x84]);

        let mut source = parse_from_string("[[bias(-0x80)]] $a:");
        let mut parser = Reader::new(&mut config);
        parser.parse(&mut source)?;
        assert_eq!(parser.bytes.offset_value, 0x104);

        let mut source = parse_from_string("[[bias(0x100)]] $a:");
        let mut parser = Reader::new(&mut config);
        parser.parse(&mut source)?;
        assert_eq!(parser.bytes.offset_value, 0);

        let mut source = parse_from_string("[[bias(-0x80)]] 84:");
        let mut parser = Reader::new(&mut config);
        parser.parse(&mut source)?;
        assert_eq!(parser.bytes.offset_value, 0x104);

        let mut source = parse_from_string("[[bias(0x100)]] 84:");
        let mut parser = Reader::new(&mut config);
        parser.parse(&mut source)?;
        assert_eq!(parser.bytes.offset_value, 0);

        check(&mut config, "}", &[]);
        check(&mut config, "{}", &[]);
        check(&mut config, "$a", &[0x84]);
        check(&mut config, "$a}", &[0x84]);
        check(&mut config, "{$a}", &[0x84]);
        check(&mut config, "84}", &[0x84]);

        Ok(())
    }

    #[test]
    fn additional_parse_tests_2() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.stoic = true;
        config.set_variable("a", &[0x84]);

        check(&mut config, "(=", &[]);
        check(&mut config, "(/2", &[]);
        check(&mut config, "$a=", &[]);
        check(&mut config, "$a=}", &[]);
        check(&mut config, "7->", &[7]);
        check(&mut config, "7->}", &[7]);

        let mut source = parse_from_string("7->$v");
        let mut parser = Reader::new(&mut config);
        parser.parse(&mut source)?;
        assert_eq!(parser.bytes.get_bytes(), vec![7u8]);
        assert_eq!(parser.config.get_variable("v").unwrap(), vec![7u8]);

        Ok(())
    }

    #[test]
    fn additional_parse_tests_3() -> ParseResult<()> {
        let mut config = HhhArgs::default();
        config.stoic = true;
        config.set_variable("a", &[0x84]);

        check(&mut config, "( $a", &[0x84]);
        check(&mut config, "( $a $a", &[0x84, 0x84]);
        check(&mut config, "( $a }", &[]);
        check(&mut config, "( 4", &[0x4]);
        check(&mut config, "( 4 )", &[0x4]);

        Ok(())
    }
}
